/**
* @Author: logansparlin
* @Company: Marlin
* @Date:   2016-08-01T18:14:59-05:00
* @Last modified by:   logansparlin
* @Last modified time: 2017-01-16T15:08:30-06:00
* @Flow
*/

// Import React & React Native
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  WebView,
  Navigator,
  NativeModules,
  Image,
  AsyncStorage,
  AppState,
  Platform
} from 'react-native';

// Import Libraries
import configureStore from './config/configureStore';
import {copyLocalFilesToDevice, saveGuideFileToCache, cacheFiles} from './lib/helpers';
import {Router, Scene, TabBar, Actions} from 'react-native-router-flux';
import {Provider, connect} from 'react-redux';
import {persistStore} from 'redux-persist';
import * as actions from './modules';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

// Import Scenes
import Layout from './Layout.js';
import ToolProductionParameters from './scenes/ToolProductionParameters';
import ProductFilter from './scenes/ProductFilter';
import GuidesFilter from './scenes/GuidesFilter';
import Products from './scenes/Products';
import Product from './scenes/Product';
import ProductOverview from './scenes/ProductOverview';
import Guides from './scenes/Guides';
import GuideViewer from './scenes/GuideViewer';
import Tools from './scenes/Tools';
import ToolConversion from './scenes/ToolConversion';
import ToolProduction from './scenes/ToolProduction';
import ToolFlockAge from './scenes/ToolFlockAge';
import CalendarModal from './scenes/CalendarModal';
import LanguageSelector from './scenes/LanguageSelector';
import ContactCobb from './scenes/ContactCobb';


// Import Components
import IconArrow from './components/IconArrow';
import TabIcon from './components/TabIcon';
import UILanguageSelector from './components/UILanguageSelector';

// Configure Store with redux persist
const store = configureStore();
const persistor = persistStore(store, {storage: AsyncStorage});
const ConnectedRouter = connect()(Router);

// This is the entry file where the app is configured and rendered
export default class App extends Component {

    componentWillMount() {
        // copy guides from MainBundlePath to DocumentDirectoryPath (only IOS, MainActivity.java handles android)
        if(Platform.OS == 'ios') {
            copyLocalFilesToDevice()
        }
        // Get network connection
        store.dispatch(actions.checkConnection())
        // restore current language from previous session or from device locale
        store.dispatch(actions.restoreLanguageSession())
        // get all of the guides (this may need to be refreshable somehow i.e. scroll to refresh)
        store.dispatch(actions.fetchGuides())
    }

    componentDidMount() {
        AppState.addEventListener('change', this.handleAppStateChange)
    }

    handleAppStateChange(appState) {
        // if app becomes active again, fetch guides
        if(appState == 'active') {
            store.dispatch(actions.fetchGuides())
        }
    }

    render() {
        return (
            <Provider store={store} persistor={persistor}>
                <ConnectedRouter
                    renderRightButton={() => <UILanguageSelector/>}
                    backButtonImage={require('./public/back_chevron.png')}
                    navigationBarStyle={[style.navBarContainer]}
                    renderTitle={() =>
                        <View style={style.navBarLogoContainer}>
                            <Image style={style.navBarLogo} source={require("./public/cobb-logo.png")} />
                        </View>
                    }>

                    <Scene key="rootTabBar" tabs={true} tabBarStyle={style.tabBarStyle} tabBarSelectedItemStyle={style.tabBarSelectedItemStyle} pressOpacity={1}>
                        <Scene
                            onPress={() => {
                                Actions.productsTab()
                                Actions.callback({ key: 'Products', type: 'reset'});
                            }}
                            key="productsTab" title="products" icon={TabIcon}>
                            <Router key="productsRouter" type="reset">
                                <Scene key="Products" component={Layout(Products)} title="Products" />
                                <Scene key="Product" component={Layout(Product)} title="Product" />
                                <Scene key="productOverview" hideTabBar component={Layout(ProductOverview)} title="Product Overview" />
                                <Scene key="guideViewerProducts" hideTabBar component={Layout(GuideViewer)} title="Guide Viewer" />
                                <Scene key="ProductFilter"
                                    panHandlers={null}
                                    direction="vertical"
                                    hideNavBar={true}
                                    component={Layout(ProductFilter)}
                                    hideTabBar />
                            </Router>
                        </Scene>
                        <Scene
                            onPress={() => {
                                Actions.guidesTab()
                                Actions.callback({ key: 'guides', type: 'reset'});
                            }}
                            key="guidesTab"
                            title="guides"
                            icon={TabIcon}>
                            <Router key="guidesRouter" type="reset">
                                <Scene key="root">
                                    <Scene key="guides" component={Layout(Guides)} title="Guides" />
                                    <Scene key="guideViewer" hideTabBar component={Layout(GuideViewer)} title="Guide Viewer" />
                                </Scene>
                                <Scene key="GuidesFilter"
                                    panHandlers={null}
                                    direction="vertical"
                                    hideNavBar={true}
                                    component={Layout(GuidesFilter)}
                                    hideTabBar />
                            </Router>
                        </Scene>
                        <Scene
                            onPress={() => {
                                Actions.toolsTab()
                                Actions.callback({ key: 'tools', type: 'reset'});
                            }}
                            key="toolsTab"
                            title="tools"
                            icon={TabIcon}>
                            <Router key="toolsRouter" type="reset">
                                <Scene key="tools" component={Layout(Tools)} title="Tools" />
                                <Scene key="ToolFlockAge" component={Layout(ToolFlockAge)} title="Flock Age Calculator" />
                                <Scene key="CalendarModal" hideNavBar={true} direction="vertical" component={Layout(CalendarModal)} title="CalendarModal" />
                                <Scene key="ToolProduction"  component={Layout(ToolProduction)} title="Production Calculator" />
                                <Scene key="ToolConversion" component={Layout(ToolConversion)} title="Conversion Calculator" />
                                <Scene key="ToolProductionParameters"
                                    panHandlers={null}
                                    component={Layout(ToolProductionParameters)}
                                    renderRightButton={null}
                                    direction="vertical"
                                    title="Production Calculator"
                                    hideTabBar
                                    hideNavBar
                                     />
                                 <Scene key="ContactCobb" component={Layout(ContactCobb)} title="Contant Cobb" />
                            </Router>
                        </Scene>
                    </Scene>
                    <Scene
                        panHandlers={null}
                        direction="vertical"
                        hideNavBar
                        key="languageSelector"
                        title="languageSelector"
                        component={LanguageSelector} />
                </ConnectedRouter>
            </Provider>
        )
    }
}

// App styles
const style = StyleSheet.create({
    tabBarStyle: {
        backgroundColor: '#f9f9f9',
        height:70,
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        zIndex: 999,
        shadowOffset: {
          height: 1,
          width: 0
        },
    },
    tabBarSelectedItemStyle: {
        backgroundColor: '#ffffff',
        flex:1,
        height:70,
        justifyContent:'space-between',
    },
    navBarLogoContainer: {
        alignSelf: 'center',
        height: 50,
        top: 25
    },
    navBarLogo: {
        width: 51,
        height: 35
    },
    navBarContainer:{
        height:80,
        backgroundColor: 'rgb(37, 35, 62)',
        borderBottomWidth:0,
        justifyContent:'flex-end',
        paddingRight:10,
        paddingLeft:10,
        paddingBottom:20,
        overflow: 'visible'
    },
    productionParametersNavBar:{
        backgroundColor:'green',
    }
})
