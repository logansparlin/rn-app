import settings from '../settings.json';
import axios from 'axios';
import store from 'react-native-simple-store';
import {NetInfo, Alert, Platform, AsyncStorage} from 'react-native';
import {getStoredState} from 'redux-persist';
import FS from 'react-native-fs';
import _ from 'underscore';

// ACTION TYPES //
const RECEIVE_GUIDES = "receive_guides";
const RECEIVE_LANGUAGES = "receive_languages";
const RECEIVE_CATEGORIES="receive_categories";
const ADD_FILTER = "add_filter";
const REMOVE_FILTER = "remove_filter";
const SET_ACTIVE_GUIDE = "set_active_guide";
const CLEAR_FILTERS = "clear_filters";

// REDUCER //

const initialState = {
    activeGuide: {},
    activeOverview: '',
    items: [],
    categories: [],
    filters: []
}

const guidesDirectory = FS.DocumentDirectoryPath + '/guides';

export default function reducer(state = initialState, action = {}) {
    switch(action.type) {
        case RECEIVE_GUIDES:
            return {
                ...state,
                items: action.payload
            }
        case ADD_FILTER:
            return {
                ...state,
                filters: [
                    ...state.filters,
                    action.payload
                ]
            }
        case REMOVE_FILTER:
            const index = state.filters.indexOf(action.payload);
            return {
                ...state,
                filters: [
                    ...state.filters.slice(0, index),
                    ...state.filters.slice(index + 1)
                ]
            }
        case CLEAR_FILTERS:
            return {
                ...state,
                filters: []
            }
        case SET_ACTIVE_GUIDE:
            return {
                ...state,
                activeGuide: action.payload
            }
        case RECEIVE_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        default:
            return state;
    }
}


// ACTIONS //

export function fetchGuides() {
    return (dispatch, getState) => {
        getStoredState({storage: AsyncStorage}, (err, state) => {
            let guides = state.guides;
            if(!guides) {
                console.log('state.guides does not exist')
                saveGuideFileToCache(dispatch)
            } else {
                console.log('state.guides does exist')
                fetchDataFromAPI(dispatch, guides.items)
            }
        })
    }
}

function fetchDataFromAPI(dispatch, guidesFromCache) {
    console.log('fetching from api')
    axios.get(settings.api_url + '/translations')
        .then(res => {
            // Check for files that aren't downloaded & download them
            checkForLocalFiles(res.data.items, guidesFromCache)
            // Update redux store with languages from API call
            dispatch({
                type: RECEIVE_LANGUAGES,
                payload: res.data.languages
            })
            // Update redux store with categories from API call
            dispatch({
                type: RECEIVE_CATEGORIES,
                payload: res.data.categories
            })
            // Update redux store with guides from API call
            return dispatch({
                type: RECEIVE_GUIDES,
                payload: res.data.items
            })
        })
        .catch(err => {
            console.log('there was an error fetching guides')
            console.log(err)
        })
}

function checkForLocalFiles(files, oldFiles) {
    files.map(file => {
        if(file.content) {
            let filePath = `${guidesDirectory}/${file.content.name}`
            let updatedFile = _.findWhere(oldFiles, {_id: file._id})

            // Check if file has been updated and download if it has
            if(updatedFile && (updatedFile.content.url !== file.content.url)) {
                console.log('file has been updated and needs to be downloaded')
                downloadFile(file.content.url, file.content.name)
            }

            // Check if file exists and download if it doesn't
            FS.stat(filePath)
                .then(file => {})
                .catch(err => {
                    downloadFile(file.content.url, file.content.name)
                })
        }
    })
}

function downloadFile(url, name) {
    let guidePath = guidesDirectory + '/' + name;
    let tempGuidePath = guidesDirectory + '/temp_' + name;
    FS.mkdir(guidesDirectory)
        .then(success => {
            let file = FS.downloadFile({
                fromUrl: url,
                toFile: tempGuidePath,
                background: true
            })


            file.promise.then(() => {
                FS.moveFile(tempGuidePath, guidePath)
                .then(success => {
                    console.log('downloaded successfully')
                })
                .catch()
            })
            .catch(err => {
                console.log('error')
                console.log(err)
            })
        });
}

function saveGuideFileToCache(dispatch) {
    let pathToGuidesFile = (Platform.OS == 'ios') ? `${FS.MainBundlePath}/guides.json` : `${FS.DocumentDirectoryPath}/guides.json`;
    FS.readFile(pathToGuidesFile, 'utf8')
        .then(data => {
            let json = JSON.parse(data);

            // Update redux store with languages from API call
            dispatch({
                type: RECEIVE_LANGUAGES,
                payload: json.languages
            })
            // Update redux store with categories from API call
            dispatch({
                type: RECEIVE_CATEGORIES,
                payload: json.categories
            })
            // Update redux store with guides from API call
            dispatch({
                type: RECEIVE_GUIDES,
                payload: json.items
            })

            return fetchDataFromAPI(dispatch, json.items)
        })
        .catch(err => {
            console.log(err)
        })
}

export function updateFilters(filter) {
    return (dispatch, getState) => {
        const filters = getState().guides.filters;
        const index = filters.indexOf(filter);
        if(index == -1) {
            dispatch({
                type: ADD_FILTER,
                payload: filter
            })
        } else {
            dispatch({
                type: REMOVE_FILTER,
                payload: filter
            })
        }
    }
}

export function clearFilters() {
    return dispatch => {
        dispatch({
            type: CLEAR_FILTERS
        })
    }
}

export function setActiveGuide(guide) {
    return dispatch => {
        dispatch({
            type: SET_ACTIVE_GUIDE,
            payload: guide
        })
    }
}
