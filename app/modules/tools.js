/**
* @Author: logansparlin
* @Date:   2017-01-04T09:16:06-06:00
* @Last modified by:   logansparlin
* @Last modified time: 2017-01-16T15:07:47-06:00
*/



// Action Types
const SELECT_DATE = "select_date";
const SELECT_WEEKS = "select_weeks";
const SELECT_DIRECTION = "select_direction";
const UPDATE_PRODUCTION_TOOL = "update_production_tool";
const RESET_PRODUCTION_TOOL = "reset_production_tool";

const initialState = {
    flockAge: {
        date: null,
        weeks: null,
        direction: 'past'
    },
    production: {
        calculateFor: 'broilers',
        pullets: 637581,
        broilers: 1500000,
        broiler_livability: 0.96,
        broiler_doa: 0.004,
        hatch_percent: 0.86,
        hatching_eggs: 155,
        pullet_livability: 0.96
    }
}

// REDUCER
export default function reducer(state = initialState, action = {}) {
    switch(action.type) {
        case SELECT_DATE:
            return {
                ...state,
                flockAge: {
                    ...state.flockAge,
                    date: action.payload
                }
            }
        case SELECT_WEEKS:
            return {
                ...state,
                flockAge: {
                    ...state.flockAge,
                    weeks: action.payload
                }
            }
        case SELECT_DIRECTION:
            return {
                ...state,
                flockAge: {
                    ...state.flockAge,
                    direction: action.payload
                }
            }
        case UPDATE_PRODUCTION_TOOL:
            return {
                ...state,
                production: action.payload
            }
        case RESET_PRODUCTION_TOOL:
            return {
                ...state,
                production: {
                    ...state.production,
                    ...action.payload
                }
            }
        default:
            return state;
    }
}

export function selectDate(date) {
    if(date.length < 1) {
        date = null
    }
    return dispatch => {
        dispatch({
            type: SELECT_DATE,
            payload: date
        })
    }
}

export function selectWeeks(weeks) {
    if(weeks.length < 1) {
        weeks = null
    }
    return dispatch => {
        dispatch({
            type: SELECT_WEEKS,
            payload: weeks
        })
    }
}

export function selectDirection(direction) {
    return dispatch => {
        dispatch({
            type: SELECT_DIRECTION,
            payload: direction
        })
    }
}

export function updateProductionTool(data) {
    return dispatch => {
        console.log(data)
        dispatch({
            type: UPDATE_PRODUCTION_TOOL,
            payload: data
        })
    }
}

export function resetProductionTool() {
    return dispatch => {
        dispatch({
            type: RESET_PRODUCTION_TOOL,
            payload: {
                broiler_livability: 0.96,
                broiler_doa: 0.004,
                hatch_percent: 0.86,
                hatching_eggs: 155,
                pullet_livability: 0.96
            }
        })
    }
}
