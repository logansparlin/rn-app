import {NetInfo} from 'react-native'

// ACTION TYPES
export const CHECK_CONNECTION = "check_connection"
export const RECEIVE_CONNECTION = "receive_connection"

// REDUCER
const initialState = {
    status: null,
    checking: false
}

export default function reducer(state = initialState, action = {}) {
    switch(action.type) {
        case RECEIVE_CONNECTION:
            return {
                status: action.payload,
                checking: false
            }
        case CHECK_CONNECTION :
            return {
                status: false,
                checking: true
            }
        default:
            return state;
    }
}

export function checkConnection() {
    return dispatch => {
        dispatch({
            type: CHECK_CONNECTION
        });

        NetInfo.isConnected.fetch()
          .done(connection => {
              dispatch(handleConnectionChange(connection))
          })

        NetInfo.addEventListener('change', connection => {
            dispatch(handleConnectionChange(connection))
        })
    }
}

function handleConnectionChange(connection) {
    return {
        type: RECEIVE_CONNECTION,
        payload: connection
    }
}
