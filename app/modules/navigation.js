import {ActionConst} from 'react-native-router-flux';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

const initialState = {
	scene: {},
    uiLanguage: ''
}

let tracker = new GoogleAnalyticsTracker('UA-88954368-1', { gLanguage: 1, uiLanguage: 2 })

// REDUCER
export default function reducer(state = initialState, action = {}) {
     // console.log(action)
    
    if (action.key === 'GuidesFilter') { 
        
        filterControlsReport('activate', 'Load Filters')
         
    } else {

        switch(action.type) {
            case 'set_active_guide': {
                console.log('nav guide')
                guideReport(action)
            }
                break;
            case 'select_product': {
                productReport(action.payload.slug, state.uiLanguage)
            }
                break;
            case ActionConst.JUMP: {
                console.log('nav jump')
                analyticsReport(action, state.uiLanguage)
            }
                break;
            case ActionConst.PUSH: {
                console.log('nav push')
                analyticsReport(action, state.uiLanguage)
            }
                break;
            case 'update_ui_language': {
                state.uiLanguage = action.payload
                tracker.trackEvent('UI Language Change', 'click-touch', {label: action.payload, value: 1});
            }
                break;
            case 'persist/REHYDRATE': {
                if(action.payload.languages && action.payload.languages.currentLanguage)
                    state.uiLanguage = action.payload.languages.currentLanguage
            }
                break;
            case 'add_filter': {
                filterReport(action, state.uiLanguage)
            }
                break;
            case 'remove_filter': {
                filterReport(action, state.uiLanguage)
            }
                break;
            case 'clear_filters': {
                filterReport(action, state.uiLanguage)
            }
                break;
            case 'update_guide_language': {
                filterReport(action, state.uiLanguage)
            }
                break;
                
        }
    }

	return state;
}

function filterReport(action, lang) {
    
    let filterAction = action.type,
        filterLabel = 'Clear Filters'
    
    filterAction = filterAction.replace(/_filter*/, '')
    
    if (action.payload) {
        filterLabel = action.payload
    }
    
    tracker.trackEvent('Filter Action', filterAction, {label: filterLabel, value: 1})
}

function filterControlsReport(actionType, actionLabel) {
    console.log('filter controls: '+actionType+' '+actionLabel)
    
    tracker.trackEvent('Filter Controls', actionType, {label: actionLabel, value: 1});
}

function guideReport(action) {

    let name = action.payload.title,
        lang = action.payload.language.value

    tracker.trackEventWithCustomDimensionValues('Guide View', 'click-touch', {label: name, value: 1}, {'gLanguage': lang});
}

function productReport(productName, lang) {

    let name = 'Products | '+productName

    tracker.trackScreenViewWithCustomDimensionValues(name, { uiLanguage: lang })
}

function analyticsReport(action, lang) {

    let actionKey = action.key,
        track = false

    if ( actionKey.includes('Tab') ) {
        actionKey = actionKey.replace('Tab', '')
        actionKey = actionKey.substring(0,1).toUpperCase() + actionKey.substring(1)
        track = true
    } else if ( actionKey.includes('Tool') ) {
        actionKey = actionKey.replace('Tool', 'Tool | ')
        track = true
    } else if (actionKey === 'ContactCobb') {
        track = true
    }

    if (track) {
        tracker.trackScreenViewWithCustomDimensionValues(actionKey, { uiLanguage: lang })
    }


}
