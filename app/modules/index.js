import { combineReducers } from 'redux';
import guidesReducer from './guides';
import connectionReducer from './connection';
import languagesReducer from './languages';
import productsReducer from './products';
import toolsReducer from './tools';
import navigationReducer from './navigation';

// export actions
export * from './guides';
export * from './connection';
export * from './languages';
export * from './products';
export * from './tools';

const rootReducer = combineReducers({
    guides: guidesReducer,
    connection: connectionReducer,
    languages: languagesReducer,
    products: productsReducer,
    tools: toolsReducer,
    navigation: navigationReducer
});

export default rootReducer
