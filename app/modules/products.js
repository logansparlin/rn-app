import _ from 'underscore';

// ACTION TYPES //
const SELECT_PRODUCT = "select_product"

// REDUCER //
const products = [
    {
        title: 'COBB500™',
        slug: 'Cobb500',
        cta: "View All COBB500TM Guides",
        overview: 'http://cobb-vantress.com/docs/default-source/cobb-500-guides/cobb500-product-overview.html',
        image: require('../public/ProductImages/product500.png')
    },
    {
        title: 'COBB700™',
        slug: 'Cobb700',
        "cta": "View All COBB700TM Guides",
        overview: 'http://cobb-vantress.com/docs/default-source/cobb-700-guides/cobb700-product-overview.html',
        image: require('../public/ProductImages/product700.png')
    },
    {
        title: 'COBBSASSO™',
        slug: 'CobbSasso',
        "cta": "View All COBBSASSOTM Guides",
        overview: 'http://cobb-vantress.com/docs/default-source/cobb-700-guides/cobb700-product-overview.html',
        image: require('../public/ProductImages/productSasso.png')
    }
]

const initialState = {
    currentProduct: products[0],
    products: products
}

export default function reducer(state = initialState, action = {}) {
    switch(action.type) {
        case SELECT_PRODUCT:
            return {
                ...state,
                currentProduct: action.payload
            }
        default:
            return state;
    }
}


// ACTIONS //
export function selectProduct(product) {
    return dispatch => {
        dispatch({
            type: SELECT_PRODUCT,
            payload: _.findWhere(products, {slug: product})
        })
    }
}
