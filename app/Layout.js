import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  WebView,
  Navigator,
  StatusBar
} from 'react-native';

export default function(ComposedComponent) {
    return class Layout extends Component {
        render() {
            return (
                <View style={styles.wrapper}>
                    <StatusBar barStyle="light-content" />
                    <View style={styles.container}>
                        <ComposedComponent />
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
  wrapper: {
      flexDirection: 'row',
      flex: 1,
  },
  container: {
      flexDirection: 'row',
      flex: 1,
  }
})
