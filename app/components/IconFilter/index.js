import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'


export default class IconFilter extends Component {
  render() {
    return (
      <View style={iconfilter.container}>
        <View style={[iconfilter.linecontainer, {paddingLeft:5}]}>
          <View style={iconfilter.dot} />
        </View>
        <View style={[iconfilter.linecontainer, {paddingLeft:15}]}>
          <View style={iconfilter.dot} />
        </View>
        <View style={[iconfilter.linecontainer, {paddingLeft:3}]}>
          <View style={iconfilter.dot} />
        </View>
      </View>
    )
  }
}

let icon = 35;
let iconcolor = '#4A4A4A';

let lineheight = ( icon / 5 );

export const iconfilter = StyleSheet.create({
  container: {
    height: icon,
    width: icon,
    maxWidth: icon,
    flex: 1,
    padding: ( icon / 10 ),
  },
  linecontainer: {
    flex:1,
    backgroundColor:iconcolor,
    justifyContent: 'center',
    marginTop: ( lineheight / 2 ),
    marginBottom: ( lineheight / 2 ),
  },
  dot:{
    width: lineheight,
    height: lineheight,
    alignItems: 'center',
    borderRadius: ( 100 / 2 ),
    borderWidth: ( lineheight / 4 ),
    borderColor: iconcolor,
    backgroundColor: 'white',
    zIndex: 999,
  }
});
