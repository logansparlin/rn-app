import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'


export default class Checkbox extends Component {
    state = {
        selected: false,
    }

    static propTypes = {
        selected: React.PropTypes.bool
    }

    static defaultProps = {
        selected: false
    }

    toggleSelected(selected){
        console.log('Selected')
        this.setState({
            selected: !this.state.selected,
        })
    }

    renderSelected(){
      if(this.props.selected){
          return (
              <View style={iconUILanguageSelector.inner} />
          )
      }

      return
    }

    render() {
        return (
            <View>
                <View style={iconUILanguageSelector.container}>
                    <View style={iconUILanguageSelector.outer}>
                        {this.renderSelected()}
                    </View>
                </View>
            </View>
        )
    }
}

let icon = 30;


const iconUILanguageSelector = StyleSheet.create({
  container: {
    justifyContent:'center',
    alignItems:'center'
  },
  outer:{
    marginRight: 8,
    height: 15,
    width: 15,
    borderRadius: 100/2,
    borderWidth : 2,
    borderColor:'#4A4A4A',
    justifyContent:'center',
    alignItems:'center'
  },
  inner:{
    height: (icon / 5),
    width: (icon / 5),
    borderRadius: 100/2,
    backgroundColor:'#4A4A4A',
  },
});
