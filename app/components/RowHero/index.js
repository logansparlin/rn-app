import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main } from '../../lib/global_styles'
import IconArrow from '../IconArrow';
import {Actions} from 'react-native-router-flux';


export default class RowHero extends Component {
    static propTypes = {
        title: React.PropTypes.string.isRequired,
        cta: React.PropTypes.string.isRequired,
        onPress: React.PropTypes.func
    }

    render(props) {
        let {title, cta} = this.props;
        return (
            <View style={[rowhero.wrapper, Main.cardshadow]}>

                <View style={rowhero.imagecontainer}>
                    <Image style={rowhero.backgroundimage} source={require('./../../public/BackgroundImages/row-hero-bkg.png')}>
                        <View style={rowhero.labelcontainer}>
                            <Text style={rowhero.label}>{title}</Text>
                        </View>
                    </Image>
                    <Image style={rowhero.productimage} source={require('./../../public/ProductImages/product500.png')}/>
                </View>

                <TouchableOpacity activeOpacity={1} style={rowhero.copycontainer} onPress={this.props.onPress}>
                    <Text style={rowhero.copylabel}>
                        {cta}
                    </Text>
                    <IconArrow />
                </TouchableOpacity>

            </View>
        )
    }
}

let rowHeight = 225;
let productheight = 190;
export const rowhero = StyleSheet.create({
    wrapper:{
        backgroundColor:'white',
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'column',
        height: rowHeight,
        borderWidth: 1,
        borderColor: '#E8E8E8',
        justifyContent:'space-between',
    },
    imagecontainer:{
        flex:.66,
        overflow:'hidden',
        justifyContent:'center',
    },
    backgroundimage:{
        flex:1,
        justifyContent:'center',
        width: null,
        height: null,
        resizeMode: 'cover',
        transform:[{
            scale: 1.2,
        }]
    },
    productimage:{
        height: productheight,
        width:(productheight / 1.2),
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:25,
        right: 25,
    },
    labelcontainer:{
        height: 70,
        backgroundColor:'rgba(209,31,62,.85)',
        justifyContent:'center',
        alignItems:'center',
    },
    label:{
        fontSize:22,
        color:'white',
        fontWeight: 'bold',
        right: 60,
    },
    copycontainer:{
        flex: .33,
        paddingLeft: 20,
        marginRight: 20,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    copylabel: {
        fontSize: 16,
        color:'#4A4A4A',
    },
});
