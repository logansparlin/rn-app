import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'
import {Actions} from 'react-native-router-flux';
import IconFilter from '../IconFilter/index.js'
import FilterContainer from '../FilterContainer/index.js'



export default class FilterButton extends Component {
    constructor() {
        super();
            this.state = {
                open: false,
            }
    }

    static defaultProps = {
        label: 'Filter Guides'
    }

    toggleFilter(open){
        console.log('Filter is Open')
        this.setState({
            open: !this.state.open
        })
    }

    renderFilterContainer(){
        if(this.state.open){
            return(
                <FilterContainer />
            )
        }
    }

    render(props) {
            return (
              <View style={buttonFilter.BottomNavBarContainer}>
                <TouchableHighlight onPress={this.props.onPress}>
                  <View style={buttonFilter.container}>
                    <Text style={buttonFilter.label}>{ this.props.label.toUpperCase()}</Text>
                    <Image style={buttonFilter.filterIcon}source={require('./../../public/Icons/filterIcon.png')}/>
                  </View>
                </TouchableHighlight>
              </View>
            )
        }
    }

export const buttonFilter = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection:'row',
    },
    label: {
        color: 'black',
        textAlign:'left',
        fontSize: 18,
        flex: 1,
        fontWeight: 'bold',

    },
    filterIcon:{
        width:40,
        height:40,
    }
});
