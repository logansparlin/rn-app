import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    ListView,
    WebView,
    Image,
    TouchableOpacity
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'
import IconArrow from '../IconArrow/index.js'


export default class RowBasic extends Component {
    static propTypes = {
        label: React.PropTypes.string.isRequired
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} activeOpacity={0.8} style={[styles.RowBasic, Main.cardshadow]}>
                <Text style={styles.RowBasicLabel}>
                    {this.props.label}
                </Text>
                <View style={styles.IconContainer}>
                    <IconArrow />
                </View>
            </TouchableOpacity>
        )
    }
}

export const styles = StyleSheet.create({
    RowBasic:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        backgroundColor:'white',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 20,
        marginRight: 20,
        padding:20,
        borderWidth: 1,
        borderColor: '#E8E8E8',
    },
    RowBasicLabel: {
        flex: 1,
        fontSize: 14,
        color:'#4A4A4A'
    },
    IconContainer: {

    }
});
