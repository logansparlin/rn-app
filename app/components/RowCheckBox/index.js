import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main, cheveron } from '../../lib/global_styles'
import IconArrow from '../IconArrow'
import Checkbox from '../Checkbox';


export default class RowCheckbox extends Component {

    static propTypes = {
        label: React.PropTypes.string,
        selected: React.PropTypes.bool,
        onPress: React.PropTypes.func
    }

    static defaultProps = {
        label: "Basic Row",
        selected: false
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={this.props.onPress} >
                <View style={rowCheckbox.container}>
                    <Checkbox selected={this.props.selected}/>
                    <Text style={rowCheckbox.label}>
                        {this.props.label}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export const rowCheckbox = StyleSheet.create({
  container:{
    marginTop:8,
    marginBottom:8,
    marginLeft:4,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-start',
  },
  label: {
    fontSize: 16,
    color:'#4A4A4A',
  },
});
