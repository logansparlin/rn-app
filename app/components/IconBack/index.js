import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'

import IconArrow from '../IconArrow/index.js'

export default class IconBack extends Component {
  render() {
    return (
      <TouchableHighlight >
        <View style={iconback.container}>
          <IconArrow color="white" direction="left"/>
          <Text style={iconback.label}>Back</Text>
        </View>
      </TouchableHighlight>
    )
  }
}

export const iconback = StyleSheet.create({
  container:{
    flexDirection:'row',
    alignItems:'center',
  },
  label: {
    color: 'white',
    textAlign:'center',
  },
});
