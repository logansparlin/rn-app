import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main, cheveron } from '../../lib/global_styles/index.js'
import IconArrow from '../IconArrow/index.js'




export default class Results extends Component {
    static propTypes = {
        title: React.PropTypes.string.isRequired,
        cta: React.PropTypes.string.isRequired,
        amount: React.PropTypes.string.isRequired,
    }
    render() {
        let {title, cta, amount} = this.props;
        return (
            <View>
                <View style={styles.container}>
                    <View style={styles.labelContainer}>
                        <Text style={styles.labeltype}>{title}</Text>
                        <Text style={styles.labelsub}>{cta}</Text>
                    </View>
                    <View style={styles.resultContainer}>
                        <Text style={styles.labelamount}>{amount}</Text>
                    </View>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        minHeight: 70,
    },
    labelContainer: {
        flex: 1.5
    },
    resultContainer: {
        flex: 1
    },
    labeltype:{
        fontSize:14,
        fontWeight: 'bold'
    },
    labelsub:{
        fontSize:12,
        fontStyle: 'italic'
    },
    labelamount:{
        fontSize: 16,
        textAlign: 'right',
    }

});
