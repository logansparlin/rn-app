import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight,
  Modal
} from 'react-native';

import { Main, ListViewBasic } from '../../lib/global_styles/index.js'

import RowCheckbox from '../RowCheckbox'



export default class UILanguageField extends Component {
  constructor(props) {
    super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        dataSource: ds.cloneWithRows(props.languages),
      };
  }

  handlePress(iso) {
      console.log(iso)
      this.props.updateUILanguage(iso)
      this.props.close()
  }

  render() {
    return (
      <ScrollView style={uilanguagefield.container} showsVerticalScrollIndicator={false}>
            {this.props.languages.map(data => {
                return (
                    <View key={data.iso_code} style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingLeft:10}}>
                        <RowCheckbox
                            label={data.native_value}
                            selected={(this.props.currentLanguage == data.iso_code) ? true : false}
                            onPress={this.handlePress.bind(this, data.iso_code)} />
                    </View>
                )
            })}
      </ScrollView>
    )
  }
}



export const uilanguagefield = StyleSheet.create({
  container:{
      flex:1,
      alignSelf: 'stretch',


  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: '#979797',
  },
  header:{
    padding: 10,
  }

});
