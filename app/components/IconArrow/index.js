import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'


export default class IconArrow extends Component {
  render() {
    return (
      <View style={rightarrow.container}>
        <View style={[{
              borderRightColor: this.props.color || "#E8E8E8",
              borderLeftColor: this.props.color || "#E8E8E8",
              borderTopColor: this.props.color || "#E8E8E8",
              borderBottomColor: this.props.color || "#E8E8E8"},
              rightarrow[this.props.direction || "right"],
              rightarrow.arrow,
            ]
          }
        />
      </View>
    )
  }
}



export const rightarrow = StyleSheet.create({
  container: {
    height:30,
    width:30,
    backgroundColor:'rgba(0,0,0,0)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  arrow: {
    height: 15,
    width: 15,
    transform: [
      {rotate: '-45deg'}
    ]
  },
  right:{
    borderRightWidth: 2,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 2,
    transform: [
      {rotate: '-45deg'}
    ]
  },
  left:{
    borderRightWidth: 0,
    borderLeftWidth: 2,
    borderTopWidth: 2,
    borderBottomWidth: 0,
    transform: [
      {rotate: '-45deg'}
    ]
  },
  up:{
    borderRightWidth: 2,
    borderLeftWidth: 0,
    borderTopWidth: 2,
    borderBottomWidth: 0,
    transform: [
      {rotate: '-45deg'}
    ]
  },
  down:{
    borderRightWidth: 0,
    borderLeftWidth: 2,
    borderTopWidth: 0,
    borderBottomWidth: 2,
    transform: [
      {rotate: '-45deg'}
    ]
  },

});
