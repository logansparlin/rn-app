import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';
import Slider from 'react-native-slider';
import { Main } from '../../lib/global_styles/index.js'
import IconArrow from '../IconArrow/'



export default class RowSlider extends Component {
    static defaultProps = {
        value: 0,
    };

    state = {
        value: this.props.value,
    };

    static propTypes = {
        type: React.PropTypes.string.isRequired,
        typeLabel: React.PropTypes.string.isRequired,
        amount: React.PropTypes.number.isRequired,
        minimumValue: React.PropTypes.number.isRequired,
        maximumValue: React.PropTypes.number.isRequired
    }


    render(props) {
        
        let {
            title, 
            type, 
            typeLabel, 
            amount, 
            limits, 
            maximumValue, 
            minimumValue, 
            minLabel, 
            maxLabel, 
            float, 
            step
        } = this.props;

        return (
            <View style={styles.wrapper}>
                <View style={styles.container}>
                    <View style={styles.copycontainer}>
                        <View style={{flex: 7}}>
                            <Text style={styles.labeltype}>{title}</Text>
                            <Text style={styles.labelsub}>{typeLabel}</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.labelAmount}>
                                {amount && (type== 'Percentage') ? (amount * 100).toFixed(float ? 2 : 0) : amount.toFixed(float ? 1 : 0)}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.container}>
                        <View>
                            <Slider
                                value={type == 'Percentage' ? amount * 100 : amount}
                                animateTransitions={true}
                                onValueChange={(val) => this.props.onSliderValueChange(type =='Percentage' ? val / 100 : val)}
                                maximumValue={ maximumValue }
                                minimumValue={ minimumValue }
                                minimumTrackTintColor="#4A4A4A"
                                maximumTrackTintColor='#d3d3d3'
                                thumbTintColor='#1a9274'
                                step={step || null}
                                trackStyle={styles.track}
                                thumbStyle={styles.thumb}/>

                            <View style={styles.valuelabels}>
                                <Text>{minimumValue}</Text>
                                <Text>{maximumValue}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export const styles = StyleSheet.create({
    wrapper:{
        minHeight:120,
        justifyContent:'center',
        borderBottomWidth: 1,
        borderColor: '#E8E8E8',
    },
    copycontainer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    labeltype:{
        fontSize:16,
    },
    labelsub:{
        fontSize:12,
    },
    labelAmount:{
        fontSize: 16,
        textAlign: 'right',
    },
    valuelabels:{
        flexDirection:'row',
        justifyContent:'space-between',

    },
    container:{
        flexDirection:'column',
        justifyContent:'center',
    },
    valueamount:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    valuelabels:{
        flexDirection:'row',
        justifyContent:'space-between',

    },
    track: {
      height: 4,
      borderRadius: 2,
    },
    thumb: {
      width: 20,
      height: 20,
      borderRadius: 20 / 2,
      backgroundColor: 'white',
      borderColor: '#4A4A4A',
      borderWidth: 2.5,
    }

});
