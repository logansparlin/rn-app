import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main, cheveron } from '../../lib/global_styles'
import IconArrow from '../IconArrow';
import {Actions} from 'react-native-router-flux';


export default class Tabs extends Component {
    constructor(){
        super();
            this.state ={
                activeTab: "future",
            }
    }

    setActiveTab(tab){
        this.props.onPress(tab)
    }

    static defaultProps = {
        activeTab: 'active',
        tabOneLabel: 'Tab 01',
        tabTwoLabel: 'Tab 02',
        tabOneValue: 'tab1',
        tabTwoValue: 'tab2'
    }

    render() {
        return (
            <View style={styles.container}>

                <TouchableOpacity activeOpacity={1} style={[styles.tabContent, (this.props.activeTab == this.props.tabOneValue ? styles.activeTab : {})]} onPress={this.setActiveTab.bind(this, this.props.tabOneValue)} >

                    <Text style={[styles.tabLabel, (this.props.activeTab == this.props.tabOneValue ? styles.activeTabLabel : {})]}>{this.props.tabOneLabel.toUpperCase()}</Text>

                    <View style={[styles.indicatorOff,(this.props.activeTab == this.props.tabOneValue ? styles.indicatorOn : {})]}/>

                </TouchableOpacity>

                <TouchableOpacity activeOpacity={1} style={[styles.tabContent, (this.props.activeTab == this.props.tabTwoValue ? styles.activeTab : {})]} onPress={this.setActiveTab.bind(this, this.props.tabTwoValue)} >

                    <Text style={[styles.tabLabel, (this.props.activeTab == this.props.tabTwoValue ? styles.activeTabLabel : {})]}>{this.props.tabTwoLabel.toUpperCase()}</Text>

                    <View style={[styles.indicatorOff,(this.props.activeTab == this.props.tabTwoValue ? styles.indicatorOn : {})]}/>

                </TouchableOpacity>


            </View>
        )
    }
}

export const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        minHeight:50,
        flex:1,
        borderBottomWidth: 1,
        justifyContent:'space-around',
        borderColor: '#E8E8E8',
    },
    tabContent:{
        flex: 1,
        alignItems:'center',
        justifyContent:'flex-end',
    },
    activeTab: {
        backgroundColor: 'white',
    },
    tabLabel:{
        color:'#AFAFAF',
        fontWeight:'bold',
        paddingBottom: 10,
    },
    activeTabLabel:{
        color:'#4A4A4A',
    },
    indicatorOn:{
        width:80,
        height: 4,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        backgroundColor:'black',
    },
    indicatorOff:{
        width:80,
        height: 4,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        backgroundColor:null,
    }

});
