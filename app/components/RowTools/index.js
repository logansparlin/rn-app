import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main, rowHero } from '../../lib/global_styles'
import IconArrow from '../IconArrow';
import {Actions} from 'react-native-router-flux';


export default class RowTools extends Component {

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        cta: React.PropTypes.string.isRequired,
        onPress: React.PropTypes.func
    }

    render() {
        let {title, cta, BkgImage} = this.props;
        return (
            <TouchableOpacity style={styles.row} activeOpacity={1}  onPress={this.props.onPress}>
                <View style={[rowHero.rowWrapper, Main.cardshadow]}>
                    <View style={rowHero.heroContainer}>
                        <Image style={rowHero.heroBackgroundImage} source={BkgImage}>
                            <View style={rowHero.heroLabel}>
                                <Text style={rowHero.toolLabel}>{title}</Text>
                            </View>
                        </Image>
                        <Image style={rowHero.toolIcon} source={require('./../../public/Icons/calculatorIconSmall.png')}/>
                    </View>
                    <View style={rowHero.copycontainer}>
                        <Text style={rowHero.copylabel}>
                            {cta}
                        </Text>
                        <IconArrow />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
  row: {
    marginLeft: 20,
    marginRight: 20,
  }
})
