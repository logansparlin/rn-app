import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableOpacity
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'

import IconFilter from '../IconFilter/index.js'


export default class ButtonUpdate extends Component {

  static defaultProps = {
        label: 'UPDATE RESULTS'
  }

  render() {
    let {disabled} = this.props;
    return (
        <TouchableOpacity
            activeOpacity={0.7}
            onPress={ !disabled ? this.props.onPress : null}
            style={[styles.container, (!disabled) ? styles.containerActive : null]}>
          <View >
            <Text style={[styles.label, (!disabled) ? styles.labelActive : null]}>{ this.props.label }</Text>
          </View>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#E8E8E8',
        paddingLeft: 20,
        paddingRight: 20,
        height:70,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'row',
    },
    label: {
        color: '#8D8D8D',
        textAlign:'left',
        fontSize: 18,
        fontWeight: 'bold',
    },
    containerActive: {
        backgroundColor: '#59EEA2'
    },
    labelActive: {
        color: 'white'
    }
});
