import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'
import IconArrow from '../IconArrow/index.js'
import IconUILanguageSelector from '../IconUILanguageSelector/index.js'


export default class RowUILanguage extends Component {
  render(props) {
    return (
      <View style={rowuilanguage.container}>
        <IconUILanguageSelector />
        <Text style={rowuilanguage.label}>
          Basic Row
        </Text>
      </View>
    )
  }
}

export const rowuilanguage = StyleSheet.create({
  container:{
    height: 20,
    marginTop:8,
    marginBottom:8,
    marginLeft: 10,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'flex-start',
  },
  label: {
    fontSize: 12,
    fontWeight: 'bold',
    color:'#4A4A4A',
  },
});
