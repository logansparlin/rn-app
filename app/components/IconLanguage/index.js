import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight
} from 'react-native';

import { Main } from '../../lib/global_styles/index.js'




export default class IconLanguage extends Component {

    static propTypes = {
        color: React.PropTypes.string,
        label: React.PropTypes.string
    }

    static defaultProps = {
        color: 'white',
        label: 'lang'
    }

    render() {
        return (
            <View style={iconlanguage.container}>
                <Text style={[{color: this.props.color}, iconlanguage.label]}>{this.props.label.toUpperCase()}</Text>
                <View>
                    <View style={[{backgroundColor: this.props.color},iconlanguage.dot]} />
                    <View style={[{backgroundColor: this.props.color},iconlanguage.dot]} />
                    <View style={[{backgroundColor: this.props.color},iconlanguage.dot]} />
                </View>
            </View>
        )
    }
}

let icon = 50;
let iconcolor = 'white';

let lineheight = 5;

export const iconlanguage = StyleSheet.create({
  container:{
    flexDirection: 'row',
    justifyContent:'flex-end',
  },
  label:{
    padding:5,
  },
  dot:{
    width: lineheight,
    height: lineheight,
    margin: 2,
    alignItems: 'center',
    borderRadius: ( 100 / 2 ),
    zIndex: 999,
  }
});
