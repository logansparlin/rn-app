import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import * as actions from '../../modules';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import Dimensions from 'Dimensions'

import { Main } from '../../lib/global_styles/index.js'

let {height, width} = Dimensions.get('window');

import IconLanguage from '../IconLanguage/index.js'
import {Actions} from 'react-native-router-flux'



class UILanguageSelector extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View>
                        <TouchableOpacity style={{width: 44, height: 44}} activeOpacity={0.8} onPress={Actions.languageSelector}>
                            <IconLanguage style={styles.test} color={this.state.open ? "black" : "white"} label={this.props.languages.currentLanguage}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    content:{
        justifyContent:'flex-start',
        alignItems:'flex-end'
    }
});

function mapStateToProps(state) {
    return {
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(UILanguageSelector)
