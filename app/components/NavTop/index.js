import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight,
  Navigator,
} from 'react-native';

import UILanguageField from '../UILanguageField/index.js'
import UILanguageSelector from '../UILanguageSelector/index.js'
import IconBack from '../IconBack/index.js'



export default class NavTop extends Component {

  render() {
    return (
      <View style={[{backgroundColor: this.props.color || "black"},navtop.container,]}>
        <IconBack />
        <IconLogo />
        {/* <UILanguageSelector /> */}
      </View>
    )
  }
}

export const navtop = StyleSheet.create({
  container: {
    maxHeight: 80,
    flex: .20,
    alignItems: 'center',
    justifyContent:'space-between',
    flexDirection:'row',
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 20,
  },
  top:{
    zIndex: 999,
  }
});


{/*
  <Navigator.NavigationBar
    style={{backgroundColor: 'gray'}}
    routeMapper={{
      LeftButton: (route, navigator, index, navState) =>
      { return (
          <Text>Cancel</Text>
         );
      },
      RightButton: (route, navigator, index, navState) =>
      { return (
         <Text>Cancel</Text>
        );
      },
      Title: (route, navigator, index, navState) =>
      { return (
        <Text>Awesome Nav Bar</Text>
        );
      },
    }}
  />
  */}
