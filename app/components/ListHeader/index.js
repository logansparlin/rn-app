import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import {Actions} from 'react-native-router-flux';

import { Main } from '../../lib/global_styles/index.js'



export default class ListHeader extends Component {
  render() {
    return (
        <View style={styles.wrapper}>
            <Image style={styles.backgroundimage} source={this.props.backgroundImage}>
                <View style={[styles.redLabelContainer, (!this.props.productImage) ? {paddingLeft: 0} : {}]}>
                    <View style={styles.copyContent}>
                        <Text style={[styles.title, !this.props.productImage ? styles.centeredText : null]}>{this.props.title || "Title"}</Text>
                        <Text style={[styles.subText, !this.props.productImage ? styles.centeredText : null]}>{this.props.subText || "Sub-Text"}</Text>
                    </View>
                </View>
                {this.props.productImage &&
                    <View style={styles.productImage}>
                        <Image style={styles.image} resizeMode={Image.resizeMode.contain} source={this.props.productImage}/>
                    </View>
                }
            </Image>
        </View>
    )
  }
}



let productheight = 150;

const styles = StyleSheet.create({
    wrapper:{
        flexDirection: 'column',
        height: 200,
        justifyContent:'space-between',
    },
    backgroundimage:{
        flex:1,
        justifyContent:'center',
        width: null,
        height: null,
        resizeMode: 'cover',
        transform:[{
            scale: 1.1,
        }]
    },
    productImage:{
        height: 130,
        width: 180,
        position:'absolute',
        zIndex: 999,
        bottom:0,
        right: 20,
        paddingRight:40,
        justifyContent:'flex-start',
        alignItems:'flex-end',
    },
    image:{
        height: productheight,
        width: (productheight / 1.3),
    },
    redLabelContainer:{
        height: 100,
        backgroundColor:'rgba(209,31,62,.85)',
        justifyContent: 'center',
        alignItems:'center',
        margin: 0,
        paddingLeft: 50,
    },
    title:{
        fontSize:20,
        left:-70,
        color:'white',
        fontWeight: 'bold',
    },
    subText:{
        left:-70,
        fontSize:14,
        color:'white',
        fontWeight: 'bold',
    },
    copylabel: {
        fontSize: 16,
        color:'#4A4A4A',
    },
    centeredText:{
        right:0,
        left: 0,
        marginLeft:30,
        marginRight: 30,
        textAlign:'center',
    },
});
