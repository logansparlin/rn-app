import React from 'react';
import {connect} from 'react-redux';
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native';

const images = {
    "guidesTab": require('../../public/Icons/guidesIcon.png'),
    "productsTab": require('../../public/Icons/productsIcon.png'),
    "toolsTab": require('../../public/Icons/toolsIcon.png')
}

const TabIcon = (props) => {
    return (
        <View style={[{opacity: (props.selected) ? 1 : .5 },tabIcon.container]}>
            <Image style={ tabIcon.iconImage } source={images[props.name]} />
            <Text style={{marginBottom:5, textAlign: 'center'}}>{props.languages.strings[props.title]}</Text>
            <View style={[{opacity: (props.selected) ? 1 : 0 },tabIcon.activeTab]}/>
        </View>
    )
}

const tabIcon = StyleSheet.create({
    container:{
        height:70,
        flexDirection:'column',
        justifyContent:'space-between',
        alignItems:'center',
    },
    iconImage:{
        flex:1,
        width: 40,
        height:40,
    },
    activeTab:{
        width:50,
        height: 4,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        backgroundColor:'black',
    }
});

function mapStateToProps(state) {
    return {
        languages: state.languages
    }
}

export default connect(mapStateToProps, null)(TabIcon);
