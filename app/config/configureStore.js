import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from '../modules';
import {autoRehydrate} from 'redux-persist';

const loggerMiddleware = logger({duration: true})

const enhancer = compose(
    autoRehydrate(),
    applyMiddleware(
      thunk,
    //   loggerMiddleware
    )
)

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    enhancer
  )
}
