import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
    WebView,
    Dimensions,
    ScrollView,
    Platform
} from 'react-native';
import PDFView from 'react-native-pdf-view';

import FS from 'react-native-fs';
const guidesDirectory = FS.DocumentDirectoryPath + '/guides';

let {width, height} = Dimensions.get('window');
import {connect} from 'react-redux';

class GuideViewer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            localFile: false
        }
    }

    componentWillMount() {
        let self = this;
        let guidePath = guidesDirectory + '/' + this.props.guides.activeGuide.content.name;
        FS.stat(guidePath)
            .then(file => {
                self.setState({
                    localFile: true
                })
            })
            // keep this line here to stop npm warning (this is kind of a hack)
            .catch(err => {
                console.log('guide does not exist')
                self.downloadFile(self.props.guides.activeGuide.content.url, this.props.guides.activeGuide.content.name)
            })
    }

    downloadFile(url, name) {
        let self = this;
        let tempFileName = guidesDirectory + '/temp_' + name;
        let fileName = guidesDirectory + '/' + name;

        let file = FS.downloadFile({
            fromUrl: url,
            toFile: tempFileName,
            background: true
        })
        file.promise.then(() => {
                FS.moveFile(tempFileName, fileName)
                .then(success => {
                    self.setState({
                        localFile: true
                    })
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    componentDiMount() {
        this.refs.webview.reload()
    }

    renderLoading() {
        return (
            <View style={styles.loadingContainer}>
                <Text style={styles.loadingText}>Loading</Text>
            </View>
        )
    }

    render() {
        let {guides: {activeGuide}} = this.props;
        console.log(this.state.localFile)
        // let guideURL = (this.state.localFile) ? guidesDirectory + '/' + activeGuide.content.name : activeGuide.content.url;
        return (
            <View style={styles.container}>
                {!this.state.loaded && !this.state.localFile ? this.renderLoading() : null}
                <ScrollView
                    minimumZoomScale={1}
                    maximumZoomScale={3}>
                    {this.state.localFile
                        ? <PDFView
                            style={styles.WebView}
                            path={guidesDirectory + '/' + activeGuide.content.name}
                            onLoadComplete={() => this.setState({loaded: true})}/>
                        : null
                    }
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        connection: state.connection,
        guides: state.guides
    }
}

export default connect(mapStateToProps, null)(GuideViewer)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 80
    },
    WebView: {
        flex: 1,
        height: height - 80,
        backgroundColor: 'green'
    },
    loadingContainer: {
        padding: 20
    },
    loadingText: {
        fontSize: 22,
        textAlign: 'center',
        color: '#333'
    }
})
