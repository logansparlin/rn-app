import React, {Component} from 'react';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    Image
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../../modules';

import { Filter } from '../../lib/global_styles'

import RowCheckbox from '../../components/RowCheckbox';

class LanguageSelector extends Component {

    handlePress(iso) {
        this.props.updateUILanguage(iso)
        Actions.pop()
    }

    render() {
        let {languages} = this.props;
        return (
            <View style={{flex: 1}}>
                <View style={Filter.navContainer}>
                    <TouchableOpacity onPress={Actions.pop} >
                        <Image style={Filter.closeButton} source={require('./../../public/Icons/closeIcon.png')} />
                    </TouchableOpacity>
                </View>
                <StatusBar barStyle='dark-content' />

                <View style={Filter.titlecontainer}>
                    <Text style={Filter.title}>{languages.strings.selectLanguage.toUpperCase()}</Text>
                </View>

                <ScrollView contentContainerStyle={{paddingBottom: 30}} style={{flex: 1}}>
                    {languages.allLanguages.map(language => {
                        return (
                            <TouchableOpacity activeOpacity={0.7} style={{padding: 10, borderBottomWidth: 2, borderBottomColor: '#eee'}} key={language.iso_code}>
                                <RowCheckbox
                                    selected={(this.props.languages.currentLanguage == language.iso_code) ? true : false}
                                    onPress={this.handlePress.bind(this, language.iso_code)}
                                    label={language.native_value.toUpperCase()} />
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(LanguageSelector)
