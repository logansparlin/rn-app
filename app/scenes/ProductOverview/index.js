import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
    WebView,
    Dimensions,
    ScrollView
} from 'react-native';

import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import {connect} from 'react-redux';

let {width, height} = Dimensions.get('window')

let tracker = new GoogleAnalyticsTracker('UA-88954368-1', { gLanguage: 1, uiLanguage: 2 })

class ProductOverview extends Component {

    componentDidMount() {
        console.log(this.props)
        let lang = this.props.languages.currentLanguage;
        let overviewName = this.props.products.currentProduct.slug + ' Overview';
        let name = 'Products | '+overviewName;

        tracker.trackScreenViewWithCustomDimensionValues(name, { uiLanguage: lang })
    }

    render() {
        let {products} = this.props;
        return (
            <View style={styles.container}>
                <ScrollView
                    minimumZoomScale={1}
                    maximumZoomScale={3}>
                    <WebView
                        ref='webview'
                        style={styles.WebView}
                        automaticallyAdjustContentInsets={false}
                        startInLoadingState={false}
                        source={{uri: products.currentProduct.overview}}
                        scalesPageToFit={false}
                    />
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        languages: state.languages,
        products: state.products
    }
}

export default connect(mapStateToProps)(ProductOverview);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    WebView: {
        top: 80,
        height: height - 80
    }
})
