/**
* @Author: logansparlin
* @Date:   2017-01-11T13:45:04-06:00
* @Last modified by:   logansparlin
* @Last modified time: 2017-01-20T10:49:16-06:00
*/



import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Animated,
  Text,
  View,
  Keyboard,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
  LayoutAnimation
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import Calendar from 'react-native-calendar';
import {Actions} from 'react-native-router-flux';
import * as actions from '../../modules';
import {connect} from 'react-redux';
import moment from 'moment';
import RNCalendarEvents from 'react-native-calendar-events';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

import { Main } from '../../lib/global_styles/'
import ListHeader from '../../components/ListHeader/'
import FilterButton from '../../components/FilterButton/'
import Tabs from '../../components/Tabs/'
import ButtonUpdate from '../../components/ButtonUpdate/'


const dayHeadings = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May',
  'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class ToolFlockAge extends Component {
    constructor(props){
        super(props);
        this.updateDirection = this.updateDirection.bind(this)
        this.addToCalendar = this.addToCalendar.bind(this)
        this.promptModal = this.promptModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.calculatedDate = null;

        this.state = {
            modalVisible: false,
            eventTitle: null,
            keyboardHeight: 0
        };
    }

    componentDidMount() {
        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardWillShow.bind(this));
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardWillHide.bind(this));
    }

    componentWillUnmount() {
      this.keyboardWillShowListener.remove();
      this.keyboardWillHideListener.remove();

    }

    _keyboardWillShow(e) {
        LayoutAnimation.configureNext({
            duration: 500,
            create: {
              type: LayoutAnimation.Types.spring,
              property: LayoutAnimation.Properties.opacity,
              springDamping: 20
            },
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 20
            },
        })

        this.setState({
            keyboardHeight: e.endCoordinates.height,
            keyboardActive: true
        })
    }

    _keyboardWillHide() {
        LayoutAnimation.configureNext({
            duration: 325,
            create: {
              type: LayoutAnimation.Types.spring,
              property: LayoutAnimation.Properties.opacity,
              springDamping: 20
            },
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 20
            },
        })

        this.setState({
            keyboardHeight: 0,
            keyboardActive: false
        })
    }

    getCalendarActiveStyles(){
        if(this.state.active){
            return{
                backgroundColor:'pink',
                height: 200,

            }
        }
    }

    updateDirection(direction) {
        console.log(direction)
        this.props.selectDirection(direction)
    }

    addToCalendar() {
        let title = this.state.eventTitle;
        let startDate = (this.props.tools.flockAge.direction.toLowerCase() == 'future')
            ? moment(this.props.tools.flockAge.date).add(this.props.tools.flockAge.weeks, 'weeks').toISOString()
            : moment(this.props.tools.flockAge.date).subtract(this.props.tools.flockAge.weeks, 'weeks').toISOString()

        let event = {
            startDate: startDate,
            endDate: startDate,
            allDay: true
        }

        RNCalendarEvents.saveEvent(title, event)
            .then(id => {
                console.log(id)
                this.closeModal()
            })
            .catch(err => {
                console.error(err)
            })



        let tracker = new GoogleAnalyticsTracker('UA-88954368-1', { gLanguage: 1, uiLanguage: 2 })
        tracker.trackEvent('Flock Age Calculator', 'add to calendar', {label: this.props.tools.flockAge.direction, value: 1});
    }

    closeModal() {
        this.setState({
            modalVisible: false
        })
    }

    promptModal() {
        let self = this;
        RNCalendarEvents.authorizeEventStore()
            .then(status => {
                if(status == 'authorized') {
                    this.setState({
                        modalVisible: true
                    });
                }
            })
            .catch(err => {
                console.error(err)
            })
    }

    renderModal() {
        let{languages} = this.props;
        return (
            <Modal
                onRequestClose={this.closeModal}
                transparent={true}
                animationType='fade'>
                <View style={{backgroundColor: 'rgba(0, 0, 0, 0.4)', flex: 1, justifyContent: 'center'}}>
                    <View style={{backgroundColor: 'white', margin: 20, paddingTop: 30, paddingBottom: 10}}>
                        <Text style={{fontSize: 22, paddingLeft: 20, paddingBottom: 20, paddingRight: 20, color: '#000'}}>{languages.strings.title}</Text>
                        <TextInput
                            onChangeText={(value) => this.setState({eventTitle: value})}
                            style={{height: 50, fontSize: 14, marginLeft: 20, marginRight: 20, paddingLeft: 20, paddingRight: 20, color: '#333', backgroundColor: '#eee'}}
                            placeholder={languages.strings.title} />
                        <View style={styles.controlContainer}>
                            <TouchableOpacity onPress={this.closeModal}>
                                <Text style={styles.cancelLabel}>{languages.strings.cancel}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.addToCalendar}>
                                <Text style={styles.saveLabel}>{languages.strings.addToCalendar}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }


    render() {
        let {tools, languages} = this.props;
        this.calculatedDate = (tools.flockAge.direction.toLowerCase() == 'future')
            ? moment(tools.flockAge.date).add(tools.flockAge.weeks, 'weeks').format("MM/DD/YYYY")
            : moment(tools.flockAge.date).subtract(tools.flockAge.weeks, 'weeks').format("MM/DD/YYYY");

        return (
            <View style={[Main.wrapperwithpadding]}>
                {this.state.modalVisible && this.renderModal()}
                <TouchableOpacity onPress={Keyboard.dismiss} style={{
                        position: 'absolute',
                        bottom: this.state.keyboardHeight,
                        backgroundColor: '#f9f9f9',
                        padding: 15,
                        zIndex: 50,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end'
                }}>
                    <Text style={{textAlign: 'right', flex: 1, color: 'blue'}}>DONE</Text>
                </TouchableOpacity>
                <KeyboardAwareScrollView extraScrollHeight={60}>
                    <ListHeader
                        productImage={null}
                        backgroundImage={require('./../../public/BackgroundImages/BlkWhite05.png')}
                        title={languages.strings.flockAgeTitle}
                        subText={languages.strings.flockAgeDescription}/>
                    <View style={[Main.containercard, styles.cardcontent, Main.cardshadow]}>
                        <Tabs
                            onPress={this.updateDirection}
                            activeTab={tools.flockAge.direction}
                            tabOneValue="past"
                            tabTwoValue="future"
                            tabOneLabel={languages.strings.past}
                            tabTwoLabel={languages.strings.future}
                            />
                        <View style={styles.inputContent}>

                            <TouchableOpacity onPress={Actions.CalendarModal}>
                                <View style={styles.calendarInput}>
                                    <Text style={[styles.calendarInputLabel, {flex:1}]}>
                                        {tools.flockAge.date ? moment(tools.flockAge.date).format('MM/DD/YYYY') : languages.strings.selectDate }
                                    </Text>
                                    <Image style={styles.calendarIcon} source={require('../../public/Icons/calendarIcon.png')} />
                                </View>
                            </TouchableOpacity>

                            <TextInput
                                keyboardType='number-pad'
                                style={[styles.textInput]}
                                placeholder={languages.strings.selectWeeks}
                                value={tools.flockAge.weeks}
                                keyboardType="numeric"
                                maxLength={4}
                                onChangeText={(value) => this.props.selectWeeks(value)}/>
                                {tools.flockAge.weeks && tools.flockAge.date && <View style={styles.resultsContainer}>
                                    <Text style={styles.weeksFromLabel}>
                                        {tools.flockAge.weeks} {(tools.flockAge.direction.toLowerCase() == languages.strings.future.toLowerCase()) ? languages.strings.weeksFrom : languages.strings.weeksBefore} {moment(tools.flockAge.date).format('MM/DD/YYYY')}
                                    </Text>
                                    <Text style={styles.dateFromLabel}>
                                        {this.calculatedDate}
                                    </Text>
                                </View>}
                            </View>
                            <ButtonUpdate
                                disabled={!tools.flockAge.weeks || !tools.flockAge.date}
                                onPress={this.promptModal}
                                label={languages.strings.addToCalendar}/>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        tools: state.tools,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(ToolFlockAge)

const styles = StyleSheet.create({
    cardcontent:{
        flex:1,
    },
    inputContent:{
        paddingTop:20,
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:30,
    },
    textInput: {
        padding: 10,
        margin:10,
        minHeight: 50,
        borderColor: '#eee',
        borderWidth: 1,
        textAlign: 'left',
        justifyContent:'center',
        alignItems:'flex-end',
        position: 'relative'
    },
    calendarInput: {
        padding: 10,
        margin:10,
        minHeight: 50,
        borderColor: '#eee',
        borderWidth: 1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    calendarInputLabel:{
        fontSize: 16,
        position: 'relative'
    },
    calendarIcon: {
        width: 35,
        height: 35
    },
    icon:{
        width: 20,
        height:20
    },
    resultsContainer:{
        marginTop: 20,
        alignItems:'center',
    },
    weeksFromLabel:{
        fontSize:16,
        marginBottom: 10,
    },
    dateFromLabel:{
        fontSize: 24,
    },
    controlContainer:{
        paddingTop: 20,
        paddingRight: 15,
        paddingLeft: 15,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    cancelLabel: {
        fontSize: 18,
        color: '#999',
        padding: 10,
        textAlign: 'left',
        alignSelf: 'flex-start'
    },
    saveLabel:{
        padding: 10,
        fontSize:18,
        color: 'blue',
        fontWeight: '500',
    }
});
