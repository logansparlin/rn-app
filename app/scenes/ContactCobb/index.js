import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    ListView,
    Linking,
    WebView,
    Image,
    TouchableOpacity
} from 'react-native';


import {connect} from 'react-redux';
import * as actions from '../../modules';
import { Main, listview} from '../../lib/global_styles';

import RowBasic from '../../components/RowBasic';

import {Actions} from 'react-native-router-flux';

import IconArrow from '../../components/IconArrow'

import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';


const data = [
    {
        "region": "Incorporated",
        "address": "PO Box 1030 Siloam Springs, Arkansas, USA 72761-1030",
        "phone": "(479) 524-3166",
        "email": "info@cobb-vantress.com",
    },
    {
        "region": "Europe Ltd",
        "address": "The Oaks, Apex 12 Old Ipswich Road, Colchester Essex CO7 7QR, UK ",
        "phone": "(479) 524-3166",
        "email": "info@cobb-vantress.com",
    },
    {
        "region": "Brazil Ltda",
        "address": "Rod Assis Chateaubriand, 10 Km. Cep: 15110-970 / POBox 2 Guapiaçu-SP-Brazil ",
        "phone": "(479) 524-3166",
        "email": "info@cobb-vantress.com",
    },
    {
        "region": "Cobb Asia-Pacific",
        "address": "Room 1002, Building A, SOHO Fuxing Plaza No. 388 Madang Road, Huangpu District Shanghai 200020, China",
        "phone": "(479) 524-3166",
        "email": "info@cobb-vantress.com",
    }
]

export default class ContactCobb extends Component {
    constructor(props) {
        super(props);
            const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            this.state = {
                dataSource: ds.cloneWithRows(data),
            };
    }

    handleClick = () => {
        console.log('clicked')
        let tracker = new GoogleAnalyticsTracker('UA-88954368-1', { gLanguage: 1, uiLanguage: 2 })
        tracker.trackEvent('Contact Cobb', 'click-touch', {label: 'External Link', value: 1})
        Linking.canOpenURL('http://www.cobb-vantress.com/footer-links/contact-us').then(supported => {
            if (supported) {
                Linking.openURL('http://www.cobb-vantress.com/footer-links/contact-us');
            } else {
                console.log('Error :(')
            }
        });
    }

    render() {
        let { region, phone, address, email} = this.props;
        return (
            <View style={[Main.wrapperwithpadding, styles.content]}>
                <ScrollView contentContainerStyle={{paddingBottom: 20}}>
                    <View style={[styles.card, Main.cardshadow]}>
                        <Text style={styles.cardLabel}>Contact Cobb</Text>
                        {data.map( (item) => {
                            return (
                                <View key={item.region} style={styles.row}>
                                    <Text style={styles.regionLabel}>{item.region}</Text>
                                    <Text>{item.phone}</Text>
                                    <Text>{item.address}</Text>
                                    <Text>{item.email}</Text>
                                </View>
                            )
                        })}
                    </View>
                    <RowBasic onPress={this.handleClick} label="www.cobb-vantress.com"/>
                </ScrollView>
            </View>
        )
    }
}




const styles = StyleSheet.create({
    card: {
        marginLeft:20,
        marginTop:20,
        marginRight:20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        paddingTop: 20,
        alignSelf:'stretch',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#eee'
    },
    row:{
        marginBottom:15,
    },
    cardLabel:{
        fontSize: 22,
        fontWeight:'bold',
        marginBottom:10,
    },
    regionLabel:{
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom:5,

    },
    socialMediaContainer:{
        flexDirection:'column',
        flexWrap:'wrap',
        flexGrow:5,
        marginTop: 5,
        marginBottom:5,
    },
    socialMediaItem:{
        flex:2,
        height:70,
        borderWidth: 1,
        borderColor: '#E8E8E8',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection:'row',
        padding:15,
        marginBottom:8,
    },
});
