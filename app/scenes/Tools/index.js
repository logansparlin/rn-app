import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight
} from 'react-native';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import { Main, listview } from '../../lib/global_styles/index.js';
import FilterButton from '../../components/FilterButton/index.js';
import RowTools from '../../components/RowTools/index.js';
import RowBasic from '../../components/RowBasic/index.js';


const data = [
    {
        "title": "conversionTitle",
        "BkgImage":require('./../../public/BackgroundImages/BlkWhite06.png'),
        "cta": "conversionDescription",
        "link": "ToolConversion"
    },
    {
        "title": "productionTitle",
        "BkgImage":require('./../../public/BackgroundImages/BlkWhite04.png'),
        "cta": "productionDescription",
        "link": "ToolProduction"
    },
    {
        "title": "flockAgeTitle",
        "BkgImage":require('./../../public/BackgroundImages/BlkWhite05.png'),
        "cta": "flockAgeDescription",
        "link": "ToolFlockAge"
    }
]


class Tools extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(data),
        };
    }

    goToRoute(link) {
        switch(link) {
            case "ToolFlockAge":
                Actions.ToolFlockAge();
                break;
            case "ToolProduction":
                Actions.ToolProduction();
                break;
            case "ToolConversion":
                Actions.ToolConversion();
                break;
            default:
                throw new Error("NO ROUTE DEFINED")
        }
    }


    render() {
        let {languages} = this.props;
        return (
            <View style={Main.wrapperwithpadding}>
                <ScrollView contentContainerStyle={[Main.insetContent, {paddingBottom: 20}]}>
                    {data.map(tool => {
                        return <RowTools
                            key={tool.title}
                            title={languages.strings[tool.title]}
                            BkgImage={tool.BkgImage}
                            cta={languages.strings[tool.cta]}
                            onPress={this.goToRoute.bind(null, tool.link)}
                        />
                    })}
                    <RowBasic 
                        label={languages.strings.contactCobb} 
                        onPress={Actions.ContactCobb} />
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        languages: state.languages
    }
}

export default connect(mapStateToProps)(Tools)