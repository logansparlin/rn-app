import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight,
  TouchableOpacity,
  LayoutAnimation,
  Animated
} from 'react-native';

// Import Libraries
import { Main, listview } from '../../lib/global_styles/index.js'
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../../modules';
import Calendar from 'react-native-calendar';
import moment from 'moment';

// Import Components
import RowSlider from '../../components/RowSlider'
import ButtonUpdate from '../../components/ButtonUpdate'


const dayHeadings = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

let {height, width} = Dimensions.get('window');

class CalendarModal extends Component {
    constructor(props){
        super(props);
        this.onDateSelect = this.onDateSelect.bind(this)
        this.state = {
            dateValue: null,
            weekvalue: null,
        };
    }

    onDateSelect(date) {
        this.props.selectDate(date)
        Actions.pop()
    }

    render() {
        let {tools, languages} = this.props;
        return (
            <View style={[styles.modalBackground]}>
                <View style={styles.card}>
                    <Calendar
                        style={styles.calendarCardContainer}
                        scrollEnabled={true}
                        showControls={true}
                        showEventIndicators={true}
                        titleFormat={'MMMM YYYY'}
                        dayHeadings={languages.strings.dayNames}
                        monthNames={languages.strings.monthNames}
                        prevButtonText={languages.strings.previous}
                        nextButtonText={languages.strings.next}
                        onDateSelect={this.onDateSelect}
                        eventDates={['2015-07-01']}
                        events={[{date:'2015-07-01'}]}
                        today={new Date()}
                        selectedDate={tools.flockAge.date}
                        customStyle={{day: {fontSize: 15, textAlign: 'center'}}}
                        weekStart={0}
                        customStyle={styles}
                     />
                     <View style={styles.controlContainer}>
                         <TouchableOpacity onPress={Actions.pop}>
                             <Text style={styles.controlLabel}>{languages.strings.cancel}</Text>
                         </TouchableOpacity>
                     </View>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        tools: state.tools,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(CalendarModal)

const styles = StyleSheet.create({
    modalBackground:{
        backgroundColor:'#eee',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    card:{
        alignSelf:'stretch',
        backgroundColor:'white',
    },
    calendarContainer:{
        backgroundColor:'white',

    },
    controlContainer:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'flex-end',
        paddingRight: 20,
    },
    controlLabel:{
        padding: 15,
        fontSize:18,
    },
    day:{
        fontSize:15,
    }
});
