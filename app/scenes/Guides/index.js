import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    ListView,
    WebView,
    Image,
    TouchableHighlight
} from 'react-native';

import data from './../data.js'
import {connect} from 'react-redux';
import * as actions from '../../modules';
import { Main, listview} from '../../lib/global_styles';
import ListHeader from '../../components/ListHeader';
import FilterButton from '../../components/FilterButton';
import RowBasic from '../../components/RowBasic';
import IconFilter from '../../components/IconFilter';
import {Actions} from 'react-native-router-flux';
import _ from 'underscore';



class Guides extends Component {

    constructor(props) {
        super(props);
        this.resetFilters = this.resetFilters.bind(this)
        const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId];
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged : (s1, s2) => s1 !== s2,
            getSectionData
        });
        this.state = {
            dataSource: ds.cloneWithRows(this.filterGuides(
                props.guides.items,
                props.guides.filters,
                props.languages
            )),
        };
    }

    resetFilters() {
        this.props.clearFilters()
        this.props.updateGuideLanguage(this.props.languages.currentLanguage)
    }

    componentWillMount() {
        this.resetFilters()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(
                this.filterGuides(
                    nextProps.guides.items,
                    nextProps.guides.filters,
                    nextProps.languages)
            )
        })
    }

    filterGuides(guides, filters, language) {
        let filteredGuides = guides.filter(guide => {
            if(filters.length >= 1) {
                return guide.language.iso_code == language.guideLanguage && _.intersection(filters, guide.categories).length >= 1;
            } else {
                return guide.language.iso_code == language.guideLanguage
            }
        })

        filteredGuides.sort((guideOne, guideTwo) => {
            return guideOne.order - guideTwo.order
        })

        return filteredGuides;
    }

    routeToGuide(guide) {
        this.props.setActiveGuide(guide);

        // EB Wrote this shit!
        Actions.guideViewer();
    }

    renderRow=(data)=> {
        return (
            <RowBasic
                label={data.title}
                onPress={this.routeToGuide.bind(this, data)} />
        )
    }


    render() {
        const {languages} = this.props;
        return (
            <View style={Main.wrapperwithpadding}>
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    showsVerticalScrollIndicator={false}
                    renderSectionHeader={() =>
                        <View style={[Main.cardshadow,{marginBottom:14}]}>
                            <FilterButton
                                label={languages.strings.filterGuides}
                                onPress={Actions.GuidesFilter} />
                        </View>}
                    renderRow={this.renderRow} />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        guides: state.guides,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(Guides)
