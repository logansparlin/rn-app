import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight,
  TouchableOpacity,
  LayoutAnimation,
  Animated
} from 'react-native';

import _ from 'underscore';
import {connect} from 'react-redux';
import * as actions from '../../modules';
import { Main, listview, Filter } from '../../lib/global_styles/index.js';
import {Actions} from 'react-native-router-flux';
import RowCheckbox from '../../components/RowCheckbox';
import ButtonUpdate from '../../components/ButtonUpdate';

const categories = ["Cobb500", "Cobb700", "CobbSasso", "Cobb MV Male", "Hatchery", "Broiler", "Breeder", "Nutrition", "Yeild", "General"]

class ProductFilter extends Component {
    constructor(props) {
      super(props);
    }

    updateFilters(filter) {
        this.props.updateFilters(filter)
    }

    changeGuideLanguage(lang) {
        this.props.updateGuideLanguage(lang)
    }

    filterIsSelected(filter) {
        return _.contains(this.props.guides.filters, filter)
    }

    languageIsSelected(lang) {
        return this.props.languages.guideLanguage == lang;
    }

    render() {
        let {
            guides: {
                items,
                filters
            },
            languages: {
                guideLanguage,
                allLanguages,
                strings}
            } = this.props;

        return (
            <View style={[Main.wrapperwithoutpadding]}>
                <View style={productFilter.navContainer}>
                    <TouchableOpacity onPress={Actions.pop} >
                        <Image style={productFilter.closeButton} source={require('./../../public/Icons/closeIcon.png')} />
                    </TouchableOpacity>
                </View>

                <View style={productFilter.titlecontainer}>
                    <Text style={productFilter.title}>{strings.filterGuides}</Text>
                </View>
                <ScrollView style={productFilter.content}>
                    <Text style={Filter.subHead}>{strings.byLanguage}:</Text>
                    <View style={productFilter.checkBoxContainer}>
                        {allLanguages.map(language =>{
                            return <RowCheckbox
                                        label={language.native_value}
                                        key={language.iso_code}
                                        onPress={this.changeGuideLanguage.bind(this, language.iso_code)}
                                        selected={this.languageIsSelected(language.iso_code)}/>
                        })}
                    </View>
                </ScrollView>
                <ButtonUpdate label={strings.updateResults} onPress={Actions.pop} />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        guides: state.guides,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(ProductFilter)

const productFilter = StyleSheet.create({
    card:{
        flex:1,
        margin: 20,
    },
    content:{
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight:20,
        paddingBottom: 0,
        flex:1,
    },
    titlecontainer:{
        borderBottomWidth: 1,
        borderColor: '#E8E8E8',
        paddingBottom:10,
        paddingLeft: 15,
        paddingRight:20,

    },
    title:{
        fontSize: 22,
        paddingLeft: 5,
    },
    subHead:{
        fontSize: 18,
        paddingLeft: 10,
    },
    navContainer:{
        justifyContent:'space-between',
        alignItems:'flex-end',
        height: 90,
        flexDirection:'row',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:20,

    },
    closeButton:{
        height: 20,
        width: 20,
    },
    restButton:{
        flex:1,
    },
    checkBoxContainer:{
        flexDirection:'column',
        flexWrap:'nowrap',
        alignItems:'flex-start',
        paddingLeft:20,
        paddingRight:20,
        marginBottom:40,

    },
    test:{
        zIndex:1000,
    }

});
