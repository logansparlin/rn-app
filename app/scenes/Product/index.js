import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Animated,
    Text,
    View,
    ScrollView,
    ListView,
    WebView,
    Image,
    TouchableHighlight,
    StatusBar,
    TouchableOpacity,
} from 'react-native';

import _ from 'underscore'
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import { Main, listview } from '../../lib/global_styles';
import * as actions from '../../modules';

import IconFilter from '../../components/IconFilter'
import ListHeader from '../../components/ListHeader'
import FilterButton from '../../components/FilterButton'
import RowBasic from '../../components/RowBasic'

class Product extends Component {

    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2
        });

        this.state = {
            dataSource: ds.cloneWithRows(
                this.filterGuides(
                    props.guides.items,
                    props.products.currentProduct.slug,
                    props.languages.guideLanguage
                )
            ),
        };
    }

    componentWillMount() {
        // Reset filters on unmount
        this.props.clearFilters()
        this.props.updateGuideLanguage(this.props.languages.currentLanguage)
    }

    componentWillReceiveProps(nextProps) {
        let newGuides = this.filterGuides(nextProps.guides.items, this.props.products.currentProduct.title)
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(
                this.filterGuides(
                    nextProps.guides.items,
                    nextProps.products.currentProduct.slug,
                    nextProps.languages.guideLanguage
                )
            )
        })
    }

    filterGuides(guides, category, guideLanguage) {
        const filteredGuides = guides.filter(guide => {
            return _.contains(guide.categories, category) && guide.language.iso_code == guideLanguage
        })

        filteredGuides.sort((guideOne, guideTwo) => {
            return guideOne.order - guideTwo.order
        })

        return filteredGuides;
    }

    routeToGuide(guide) {
        this.props.setActiveGuide(guide);
        Actions.guideViewerProducts();
    }

    render() {
        let {products, languages} = this.props;
        return (
            <View style={Main.wrapperwithpadding}>
                <ScrollView stickyHeaderIndices={[1]} showsVerticalScrollIndicator={false}>
                    <TouchableOpacity style={{overflow: 'hidden'}} onPress={Actions.productOverview} opacity={1}>
                        <ListHeader
                            productImage={products.currentProduct.image}
                            backgroundImage={require('./../../public/BackgroundImages/row-hero-bkg.png')}
                            title={products.currentProduct.title}
                            subText={languages.strings.productOverview + " >"} />
                    </TouchableOpacity>
                    <View style={[Main.cardshadow, {marginBottom:14}]}>
                        <FilterButton
                            onPress={Actions.ProductFilter}
                            icon={<IconFilter/>}
                            label={languages.strings.filterByLanguage} />
                    </View>
                    <ListView
                        style={{paddingBottom: 10}}
                        showsVerticalScrollIndicator={false}
                        dataSource={this.state.dataSource}
                        renderRow={(data) => <RowBasic label={data.title} onPress={this.routeToGuide.bind(this, data)} />}/>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet

function mapStateToProps(state) {
    return {
        products: state.products,
        guides: state.guides,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(Product)
