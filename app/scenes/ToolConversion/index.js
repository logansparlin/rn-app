/**
* @Author: logansparlin
* @Date:   2017-01-03T15:43:23-06:00
* @Last modified by:   logansparlin
* @Last modified time: 2017-01-20T10:43:18-06:00
*/



import Dimensions from 'Dimensions';
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Animated,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  LayoutAnimation
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

let {width, height} = Dimensions.get('window')

import {connect} from 'react-redux';
import _ from 'underscore';
import convert from 'convert-units';
import conversionData from './conversionData.json';
import RowCheckbox from '../../components/RowCheckbox';
import ListHeader from '../../components/ListHeader';
import { Main, ListViewBasic } from '../../lib/global_styles';


class ToolConversion extends Component {
    constructor() {
        super()
        this.updateFraction = this.updateFraction.bind(this)
    }

    state = {
        categoryData: conversionData[0],
        imperialIndex: 0,
        metricIndex: 0,
        imperialValue: '',
        metricValue: '',
        numerator: '',
        denominator: '',
        keyboardActive: false,
        keyboardHeight: 0,
        feetPerMinute: '',
        metersPerSecond: '',
        fahrenheit: '',
        celsius: '',
    }

    componentDidMount() {
        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardWillShow.bind(this));
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardWillHide.bind(this));
    }

    componentWillUnmount() {
      this.keyboardWillShowListener.remove();
      this.keyboardWillHideListener.remove();

    }

    _keyboardWillShow(e) {
        LayoutAnimation.configureNext({
            duration: 500,
            create: {
              type: LayoutAnimation.Types.spring,
              property: LayoutAnimation.Properties.opacity,
              springDamping: 20
            },
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 20
            },
        })

        this.setState({
            keyboardHeight: e.endCoordinates.height,
            keyboardActive: true
        })
    }

    _keyboardWillHide() {
        LayoutAnimation.configureNext({
            duration: 325,
            create: {
              type: LayoutAnimation.Types.spring,
              property: LayoutAnimation.Properties.opacity,
              springDamping: 20
            },
            update: {
              type: LayoutAnimation.Types.spring,
              springDamping: 20
            },
        })

        this.setState({
            keyboardHeight: 0,
            keyboardActive: false
        })
    }

    renderSelectItem() {
      let {languages} = this.props;
      return conversionData.map((data, index) => {
          let active = this.state.categoryData.category == data.category;
          return (
              <TouchableOpacity
                  activeOpacity={0.7}
                  key={data.category}
                  onPress={() => {
                      this.setState({categoryData: conversionData[index], imperialIndex: 0, metricIndex: 0, imperialValue: '', metricValue: ''})
              }}>
                  <View style={styles.selectorItemContainer}>
                      <Text style={[styles.selectorItem, active ? styles.active : null]}>{languages.strings[data.category]}</Text>
                      <View style={[styles.indicatorOff, active ? styles.indicatorOn : null]} />
                  </View>
              </TouchableOpacity>
          )
      });
    }

    handlePress(type, index) {
        let obj = {}
        let {metricValue, imperialValue} = this.state;
        obj[`${type}Index`] = index
        this.setState(obj, () => {
            this.handleImperialInputChange(imperialValue)
        })
    }

    handleAirSpeedChange(type, val) {
      let {feetPerMinute, metersPerSecond} = this.state;
      if(type == 'feetPerMinute') {
        this.setState({
          feetPerMinute: (val > 0) ? val : '',
          metersPerSecond: (val > 0) ? (val * 0.0045).toFixed(4) : ''
        })
      } else {
        this.setState({
          feetPerMinute: (val > 0) ? (val / 0.0045).toFixed(2) : '',
          metersPerSecond: (val > 0) ? val : ''
        })
      }
    }

    handleTemperatureChange(type, val) {
      let {fahrenheit, celsius} = this.state;

      if(!isNaN(val)) {
        if(type == 'fahrenheit') {
          this.setState({
            fahrenheit: val,
            celsius: convert(val).from('F').to('C').toFixed(0)
          })
        } else {
          this.setState({
            fahrenheit: convert(val).from('C').to('F').toFixed(0),
            celsius: val
          })
        }
      } else {
        this.setState({
          fahrenheit: '',
          celsius: ''
        })
      }
    }


    handleImperialInputChange(val) {
        let num = parseFloat(val)
        let imperialUnit = this.state.categoryData.units_imperial[this.state.imperialIndex].shorthand;
        let metricUnit = this.state.categoryData.units_metric[this.state.metricIndex].shorthand;
        this.setState({
            imperialValue: (num > 0) ? num : "",
            metricValue: (num > 0) ? convert(num).from(imperialUnit).to(metricUnit).toFixed(2) : ""
        })
    }

    handleMetricInputChange(val) {
        let num = parseFloat(val)
        let imperialUnit = this.state.categoryData.units_imperial[this.state.imperialIndex].shorthand;
        let metricUnit = this.state.categoryData.units_metric[this.state.metricIndex].shorthand;
        this.setState({
            metricValue: (num > 0) ? num : "",
            imperialValue: (num > 0) ? convert(num).from(metricUnit).to(imperialUnit).toFixed(2) : ""
        })
    }

    roundToTwo(num) {
        return +(Math.round(num + "e+2")  + "e-2");
    }

    updateFraction(val, type) {
        let {numerator, denominator} = this.state;
        let obj = this.state;
        obj[type] = (val <= 0) ? '' : parseInt(val)
        this.setState(obj)
    }

    renderConversionView() {
        let {numerator, denominator} = this.state;
        let {languages} = this.props;
        let fractionValue =  (numerator <= 0 || denominator <= 0) ? '' : this.roundToTwo(numerator / denominator)

        if(this.state.categoryData.category == 'fractions') {
            return (
                <View style={[styles.conversionContainer, {height:200}]}>

                    <View style={[styles.inputContainer]}>
                        <Text style={styles.label}>{languages.strings.fraction}</Text>
                        <View style={[styles.decimalContainer]}>
                            <TextInput
                                ref="textInput1"
                                onChangeText={(val) => this.updateFraction(val, 'numerator')}
                                maxLength={2}
                                style={styles.fractionInput}
                                placeholder="0"
                                returnKeyType='next'
                                keyboardType="numeric" />
                            <View style={{width:80, height:2, backgroundColor:'#eee'}} />
                            <TextInput
                                ref="textInput2"
                                onChangeText={(val) => this.updateFraction(val, 'denominator')}
                                maxLength={2}
                                style={styles.fractionInput}
                                placeholder="0"
                                keyboardType="numeric" />
                        </View>
                    </View>

                    <Text style={styles.conversionArrowContainer}>=</Text>

                    <View style={[styles.inputContainer]}>
                        <Text style={styles.label}>{languages.strings.decimal}</Text>
                        <View style={[styles.decimalContainer]}>
                            <Text style={styles.decimalResult}>{fractionValue.toString()}</Text>
                        </View>
                    </View>

                </View>
            )
        }

        if(this.state.categoryData.category == 'birds') {
            return (
                <View>
                    <View style={styles.conversionContainer}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>{languages.strings.birdsPerSquareFoot}</Text>
                            <TextInput
                                ref="textInput3" defaultValue={this.state.imperialValue.toString()} placeholder="0" onChangeText={this.handleImperialInputChange.bind(this)} keyboardType="numeric" style={styles.textInput} />
                        </View>

                        <Text style={styles.conversionArrowContainer}>=</Text>

                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>{languages.strings.birdsPerSquareMeter}</Text>
                            <TextInput
                                ref="textInput4" defaultValue={this.state.metricValue.toString()} placeholder="0" onChangeText={this.handleMetricInputChange.bind(this)} keyboardType="numeric" style={styles.textInput} />
                        </View>
                    </View>
                </View>
            )
        }

        if(this.state.categoryData.category == 'airSpeed') {
          return (
            <View style={styles.conversionContainer}>
              <View style={styles.inputContainer}>
                <Text style={styles.label}>{languages.strings.feetPerMinute}</Text>
                <TextInput
                    ref="textInput5"
                  defaultValue={this.state.feetPerMinute.toString()}
                  placeholder="0"
                  keyboardType="numeric"
                  maxLength={5}
                  style={styles.textInput}
                  onChangeText={this.handleAirSpeedChange.bind(this, 'feetPerMinute')} />
              </View>

              <Text style={styles.conversionArrowContainer}>=</Text>

              <View style={styles.inputContainer}>
                <Text style={styles.label}>{languages.strings.metersPerSecond}</Text>
                <TextInput
                    ref="textInput6"
                  defaultValue={this.state.metersPerSecond.toString()}
                  placeholder="0"
                  keyboardType="numeric"
                  maxLength={5}
                  style={styles.textInput}
                  onChangeText={this.handleAirSpeedChange.bind(this, 'metersPerSecond')} />
              </View>
            </View>
          )
        }

        if(this.state.categoryData.category == 'temperature') {
            return (
                <View>
                    <View style={styles.conversionContainer}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>{languages.strings.fahrenheit}</Text>

                            <TextInput
                                ref="textInput7"
                                defaultValue={this.state.fahrenheit.toString()}
                                placeholder="0"
                                onChangeText={this.handleTemperatureChange.bind(this, 'fahrenheit')}
                                keyboardType="numeric"
                                style={styles.textInput} />
                        </View>

                        <Text style={styles.conversionArrowContainer}>=</Text>

                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>{languages.strings.celsius}</Text>

                            <TextInput
                                ref="textInput8" defaultValue={this.state.celsius.toString()}
                                placeholder="0"
                                onChangeText={this.handleTemperatureChange.bind(this, 'celsius')}
                                keyboardType="numeric"
                                style={styles.textInput} />
                        </View>
                    </View>
                </View>
            )
        }

        return (
            <View>
                <View style={styles.conversionContainer}>
                    <View style={styles.inputContainer}>
                        <Text style={styles.label}>{languages.strings.imperial}</Text>
                        <TextInput
                            ref="textInput9"
                            defaultValue={this.state.imperialValue.toString()}
                            placeholder="0"
                            onChangeText={this.handleImperialInputChange.bind(this)}
                            keyboardType="numeric"
                            maxLength={5}
                            style={styles.textInput} />
                    </View>

                    <Text style={styles.conversionArrowContainer}>=</Text>

                    <View style={styles.inputContainer}>
                        <Text style={styles.label}>{languages.strings.metric}</Text>
                        <TextInput
                            ref="textInput10" defaultValue={this.state.metricValue.toString()} placeholder="0" onChangeText={this.handleMetricInputChange.bind(this)} keyboardType="numeric" style={styles.textInput} />
                    </View>
                </View>
                <View style={styles.unitsContainer}>
                    <View style={styles.units}>
                        {this.state.categoryData.units_imperial.map((unit, index) => {
                            let isSelected = this.state.imperialIndex == index;
                            return (
                              <RowCheckbox
                                onPress={this.handlePress.bind(this, 'imperial', index)}
                                selected={isSelected}
                                label={languages.strings[unit.value]}
                                key={unit.value} />
                            )
                        })}
                    </View>
                    <View style={styles.units}>
                        {this.state.categoryData.units_metric.map((unit, index) => {
                            let isSelected = this.state.metricIndex == index;
                            return (
                              <RowCheckbox
                                onPress={this.handlePress.bind(this, 'metric', index)}
                                selected={isSelected}
                                label={languages.strings[unit.value]}
                                key={unit.value} />
                            )
                        })}
                    </View>
                </View>
            </View>
        )
    }

    render() {
      let {languages} = this.props;
        return (
            <View style={Main.wrapperwithpadding}>
                    <TouchableOpacity onPress={Keyboard.dismiss} style={{
                            position: 'absolute',
                            bottom: this.state.keyboardHeight,
                            backgroundColor: '#f9f9f9',
                            padding: 15,
                            zIndex: 50,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-end'
                    }}>
                        <Text style={{textAlign: 'right', flex: 1, color: 'blue'}}>DONE</Text>
                    </TouchableOpacity>
                <KeyboardAwareScrollView
                    extraScrollHeight={60}
                    keyboardDismissMode='on-drag'
                    keyboardShouldPersistTaps={true}>
                    <ListHeader
                        productImage={null}
                        backgroundImage={require('./../../public/BackgroundImages/BlkWhite06.png')}
                        title={languages.strings.conversionTitle}
                        subText={languages.strings.conversionDescription}
                        />
                    <View style={[styles.card, Main.cardshadow]}>
                        <ScrollView style={styles.scrollSelector} horizontal={true} showsHorizontalScrollIndicator={false}>
                            {this.renderSelectItem()}
                        </ScrollView>
                        {this.renderConversionView()}
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
  return {
    languages: state.languages
  }
}

export default connect(mapStateToProps)(ToolConversion)

const styles = StyleSheet.create({
  top: {
    zIndex:999,
  },
  card: {
      margin: 20,
      backgroundColor: 'white',
      borderWidth: 1,
      borderColor: '#eee'
  },
  scrollSelector: {
      borderBottomWidth: 1,
      borderBottomColor: '#eee',
  },
  selectorItem: {
      paddingLeft: 15,
      paddingRight: 15,
      paddingBottom: 10,
      fontWeight: 'bold',
      fontSize: 14,
      color: '#999'
  },
  active: {
      color: 'black'
  },
  labelContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 20
  },
  label: {

      textAlign: 'center',
      paddingBottom:10,
  },
  conversionContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
  },
  fractionContainer: {
      flexDirection: 'column',
      flex: 1,
      borderWidth: 2,
      borderColor: '#eee',
  },
  fractionInput: {
      flex: 1,
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      height: 50,
      marginBottom: 5,
      textAlign: 'center',
      alignSelf: 'stretch'
  },
  inputContainer: {
      flex: 1,
      margin:10,
      alignItems: 'center',
      justifyContent: 'center',
  },
  textInput: {
      flex: 1,
      height: 70,
      alignSelf:'stretch',
      borderColor: '#eee',
      borderWidth: 1,
      textAlign: 'center',
  },
  unitsContainer: {
      flexDirection: 'row',
      paddingLeft: 20,
      paddingRight: 20,
      paddingBottom: 20
  },
  units: {
      flex: 1
  },
  indicatorOn:{
      flex:1,
      marginRight:10,
      marginLeft: 10,
      height: 4,
      borderTopLeftRadius:30,
      borderTopRightRadius:30,
      backgroundColor:'black',
  },
  indicatorOff:{
      flex:1,
      marginRight:10,
      marginLeft: 10,
      height: 4,
      borderTopLeftRadius:30,
      borderTopRightRadius:30,
      backgroundColor:null,
  },
  selectorItemContainer:{
      paddingTop: 15,
      justifyContent:'flex-end',
  },
  decimalContainer: {
      flex: 1,
      height: 70,
      alignSelf:'stretch',
      alignItems:'center',
      justifyContent:'center',
      borderColor: '#eee',
      borderWidth: 1,
  },
  decimalInput: {
      flex: 1,
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      padding: 0
  },
  decimalResult: {
      fontSize: 22,
      color: '#000',
      height: 40,
  },
  decimalLabel: {
      fontSize: 12,
      color: '#999'
  },
  border:{
      borderColor: '#eee',
      borderWidth: 1,
  },
  conversionArrow:{
      height:10,
      width:10,
  },
  conversionArrowContainer:{
      top:12,
  }
});
