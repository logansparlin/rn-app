import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight,
  TouchableOpacity,
  LayoutAnimation,
  Animated
} from 'react-native';

import _ from 'underscore';
import {connect} from 'react-redux';
import * as actions from '../../modules';
import { Main, listview, Filter } from '../../lib/global_styles/index.js';
import {Actions} from 'react-native-router-flux';
import RowCheckbox from '../../components/RowCheckbox';
import ButtonUpdate from '../../components/ButtonUpdate';

class GuidesFilter extends Component {
    constructor(props) {
      super(props);
    }

    updateFilters(filter) {
        this.props.updateFilters(filter)
    }

    changeGuideLanguage(lang) {
        this.props.updateGuideLanguage(lang)
    }

    filterIsSelected(filter) {
        return _.contains(this.props.guides.filters, filter)
    }

    languageIsSelected(lang) {
        return this.props.languages.guideLanguage == lang;
    }

    resetFilters() {
        this.props.clearFilters()
        this.props.updateGuideLanguage(this.props.languages.currentLanguage)
        Actions.pop()
    }

    render() {
        let {
            guides: {
                items,
                filters,
                categories
            },
            languages: {
                guideLanguage,
                allLanguages,
                strings
            }
        } = this.props;

        return (
            <View style={[Main.wrapperwithoutpadding]}>
                <View style={Filter.navContainer}>
                    <TouchableOpacity onPress={Actions.pop} >
                        <Image style={Filter.closeButton} source={require('./../../public/Icons/closeIcon.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.resetFilters.bind(this)}>
                        <Text style={Filter.resetButton}>{strings.resetAll.toUpperCase()}</Text>
                    </TouchableOpacity>
                </View>
                <View style={Filter.titlecontainer}>
                    <Text style={Filter.title}>{strings.filterGuides}</Text>
                </View>
                <ScrollView style={Filter.content}>
                    <Text style={Filter.subHead}>{strings.byCategory}:</Text>
                    <View style={Filter.checkBoxContainer}>
                        {categories.map(category =>{
                            return <RowCheckbox
                                        label={category}
                                        key={category}
                                        onPress={this.updateFilters.bind(this, category)}
                                        selected={this.filterIsSelected(category)} />
                        })}
                    </View>
                    <Text style={Filter.subHead}>{strings.byLanguage}:</Text>
                    <View style={Filter.checkBoxContainer}>
                        {allLanguages.map(language =>{
                            return <RowCheckbox
                                        label={language.native_value.toUpperCase()}
                                        key={language.iso_code}
                                        onPress={this.changeGuideLanguage.bind(this, language.iso_code)}
                                        selected={this.languageIsSelected(language.iso_code)}/>
                        })}
                    </View>
                </ScrollView>
                <ButtonUpdate label={strings.updateResults} onPress={Actions.pop} />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        guides: state.guides,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(GuidesFilter)
