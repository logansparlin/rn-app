import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as actions from '../../modules'
import {
    View,
    Text,
    ListView,
    StyleSheet
} from 'react-native'
import { Main, TextStyles } from '../../lib/global_styles/index.js'
import RowBasic from '../../components/RowBasic/index.js'

class GuidesTest extends Component {

    constructor(props) {
        super(props)

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
          dataSource: ds.cloneWithRows([]),
        };
    }

    componentWillMount() {
        this.props.fetchGuides()
        this.props.checkConnection()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.guides)
        })
    }

    renderRow(data) {
        return (
            <View>
                <Text>{data.title}</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={Main.wrapper}>
            <Text style={{textAlign: 'center', padding: 20}}>DATA:</Text>
            <Text style={{paddingRight: 20, paddingLeft: 20, fontSize: 18, fontWeight: 'bold'}}>Connection: {this.props.connection.status ? 'Connected' : 'No Connection'}</Text>
            <ListView
                enableEmptySections={true}
                dataSource={this.state.dataSource}
                renderRow={this.renderRow}
            />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        guides: state.guides,
        connection: state.connection
    }
}

export default connect(mapStateToProps, actions)(GuidesTest)
