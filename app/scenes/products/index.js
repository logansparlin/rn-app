import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';

import { Main, listview, rowHero } from '../../lib/global_styles'

import FilterButton from '../../components/FilterButton/'
import RowHero from '../../components/RowHero/'
import IconArrow from '../../components/IconArrow/'

import {Actions} from 'react-native-router-flux';
import * as actions from '../../modules'
import {connect} from 'react-redux';

const data = [
    {
        "title": "COBB500™",
        "cta": "View All COBB500™ Guides",
        "link": "Product",


    },
    {
        "title": "COBB700™",
        "cta": "View All COBB700™ Guides",
        "link": "Product",


    },
    {
        "title": "COBBSASSO™",
        "cta": "View All COBBSASSO™ Guides",
        "link": "Product",

    }
]


class Products extends Component {
  constructor(props) {
    super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        dataSource: ds.cloneWithRows(data),
      };
  }

  goToRoute(product) {
      this.props.selectProduct(product)
      Actions.Product()
  }

  render() {
    let {languages} = this.props;
    return (
        <View style={Main.wrapperwithpadding}>
            <ScrollView>
                <View style={[listview.container, Main.insetContent]}>

                    {/*--Cobb500 Product Row--*/}
                    <TouchableOpacity activeOpacity={1}  onPress={this.goToRoute.bind(this, "Cobb500")}>
                        <View style={[rowHero.rowWrapper, Main.cardshadow]}>
                            <View style={rowHero.heroContainer}>
                                <Image style={rowHero.heroBackgroundImage} source={require('./../../public/BackgroundImages/BlkWhite01.png')}>
                                    <View style={rowHero.heroLabel}>
                                        <Text style={rowHero.labelRight}>{data[0].title}</Text>
                                    </View>
                                </Image>
                                <Image style={rowHero.productimage} source={require('./../../public/ProductImages/product500.png')}/>
                            </View>
                            <View style={rowHero.copycontainer}>
                                <Text style={rowHero.copylabel}>
                                    {languages.strings.viewAll500}
                                </Text>
                                <IconArrow />
                            </View>
                        </View>
                    </TouchableOpacity>

                    {/*--Cobb700 Product Row--*/}
                    <TouchableOpacity activeOpacity={1}  onPress={this.goToRoute.bind(this, "Cobb700")}>
                        <View style={[rowHero.rowWrapper, Main.cardshadow]}>
                            <View style={rowHero.heroContainer}>
                                <Image style={rowHero.heroBackgroundImage} source={require('./../../public/BackgroundImages/BlkWhite02.png')}>
                                    <View style={[rowHero.heroLabel, rowHero.flexEnd]}>
                                        <Text style={rowHero.labelLeft}>{data[1].title}</Text>
                                    </View>
                                </Image>
                                <Image style={rowHero.cobb700} source={require('./../../public/ProductImages/product700.png')}/>
                            </View>
                            <View style={rowHero.copycontainer}>
                                <Text style={rowHero.copylabel}>
                                    {languages.strings.viewAll700}
                                </Text>
                                <IconArrow />
                            </View>
                        </View>
                    </TouchableOpacity>

                    {/*--CobbSasso Product Row--*/}
                    <TouchableOpacity activeOpacity={1}  onPress={this.goToRoute.bind(this, "CobbSasso")}>
                        <View style={[rowHero.rowWrapper, Main.cardshadow]}>
                            <View style={rowHero.heroContainer}>
                                <Image style={rowHero.heroBackgroundImage} source={require('./../../public/BackgroundImages/BlkWhite03.png')}>
                                    <View style={rowHero.heroLabel}>
                                        <Text style={rowHero.labelRight}>{data[2].title}</Text>
                                    </View>
                                </Image>
                                <Image style={rowHero.cobbSasso} source={require('./../../public/ProductImages/productSasso.png')}/>
                            </View>
                            <View activeOpacity={1} style={rowHero.copycontainer} onPress={this.props.onPress}>
                                <Text style={rowHero.copylabel}>
                                    {languages.strings.viewAllSasso}
                                </Text>
                                <IconArrow />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
  }
}

function mapStateToProps(state) {
    return {
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(Products);
