/**
* @Author: logansparlin
* @Date:   2017-01-04T09:16:06-06:00
* @Last modified by:   logansparlin
* @Last modified time: 2017-01-16T15:03:42-06:00
*/



import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Animated,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  TouchableHighlight,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import * as actions from '../../modules';

import Slider from 'react-native-slider';
import { Main, listview } from '../../lib/global_styles'
    import FilterButton from '../../components/FilterButton'
import Results from '../../components/Results'
import Tabs from '../../components/Tabs'

import ListHeader from '../../components/ListHeader'
import IconFilter from '../../components/IconFilter'


class ToolProduction extends Component {

    constructor() {
        super()
        this.changeTab = this.changeTab.bind(this)
        this.onSliderValueChange = this.onSliderValueChange.bind(this)
        this.getCalculations = this.getCalculations.bind(this)
        this.state = {
            sliderValue: 2500000
        }
    }

    changeTab(tab) {
        // Update redux state with new production values
        this.props.updateProductionTool({
            ...this.props.tools.production,
            calculateFor: tab
        })
    }

    numberWithCommas(num) {
        return Math.round(num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    onSliderValueChange(pullets, val) {
        this.setState({sliderValue: val})
    }

    getCalculations() {
        let productionTool = this.props.tools.production;
        let {sliderValue} = this.state;

        let broilersPerYear;
        let broilerChicksPlaced;
        let hatchingEggs;
        let hens;
        let pullets;

        if(productionTool.calculateFor == 'pullets') {
            pullets = sliderValue;
            hens = pullets * productionTool.pullet_livability;
            hatchingEggs = productionTool.hatching_eggs * hens;
            broilerChicksPlaced = hatchingEggs * productionTool.hatch_percent;
            broilersPerYear = broilerChicksPlaced * (productionTool.broiler_livability - productionTool.broiler_doa);
            broilers = broilersPerYear / 52;
        } else if(productionTool.calculateFor == 'broilers') {
            broilers = sliderValue;
            broilersPerYear = sliderValue * 52;
            broilerChicksPlaced = broilersPerYear / (productionTool.broiler_livability - productionTool.broiler_doa);
            hatchingEggs = broilerChicksPlaced / productionTool.hatch_percent;
            hens = hatchingEggs / productionTool.hatching_eggs;
            pullets = hens / productionTool.pullet_livability;
        }

        return {
            broilers,
            broilersPerYear,
            broilerChicksPlaced,
            hatchingEggs,
            hens,
            pullets
        }
    }

    render() {
        let {tools: {production}, languages} = this.props;

        let calculations = this.getCalculations()

        let {sliderValue} = this.state;

        return (
            <View style={Main.wrapperwithpadding}>
                <ScrollView >
                    <ListHeader
                        productImage={null}
                        backgroundImage={require('./../../public/BackgroundImages/BlkWhite04.png')}
                        title={languages.strings.productionTitle}
                        subText={languages.strings.productionDescription}/>
                    <View style={[styles.card, Main.cardshadow]}>
                        <Tabs
                            onPress={this.changeTab}
                            activeTab={production.calculateFor}
                            tabOneValue="broilers"
                            tabTwoValue="pullets"
                            tabOneLabel={languages.strings.broilers}
                            tabTwoLabel={languages.strings.pullets} />

                        <View style={[styles.filterButton,listview.container]}>
                            <Text style={styles.mainSliderValue}>
                                {this.numberWithCommas(sliderValue)}
                            </Text>
                            <Text style={styles.mainSliderSubTitle}>
                                ({(production.calculateFor == 'pullets') ? languages.strings.perYear : languages.strings.perWeek})
                            </Text>
                            <Slider
                                value={parseInt(sliderValue)}
                                animateTransitions={true}
                                onValueChange={this.onSliderValueChange.bind(this, this.pullets)}
                                maximumValue={ 5000000 }
                                minimumValue={ 100000 }
                                minimumTrackTintColor="#4A4A4A"
                                maximumTrackTintColor='#d3d3d3'
                                thumbTintColor='#1a9274'
                                trackStyle={styles.track}
                                thumbStyle={styles.thumb}
                                step={100000}/>
                            <View style={styles.valuelabels}>
                                <Text>100k</Text>
                                <Text>5.00m</Text>
                            </View>
                        </View>

                        <View style={styles.filterButton}>
                            <FilterButton
                                onPress={Actions.ToolProductionParameters}
                                label={languages.strings.parameters}
                                style={styles.filterButton}
                                icon={<IconFilter/>} />
                        </View>

                        {(production.calculateFor == 'broilers')
                            ?   <View style={listview.container}>
                                    <Results
                                        title={languages.strings.broilers}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.broilersPerYear)}
                                        />
                                    <Results
                                        title={languages.strings.chicksPlaced}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.broilerChicksPlaced)}
                                        />
                                    <Results
                                        title={languages.strings.hatchingEggs}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.hatchingEggs)}
                                        />
                                    <Results
                                        title={languages.strings.hensHoused}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.hens)}
                                        />
                                </View>
                            :   <View style={listview.container}>
                                    <Results
                                        title={languages.strings.hensHoused}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.hens)}
                                        />
                                    <Results
                                        title={languages.strings.hatchingEggs}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.hatchingEggs)}
                                        />
                                    <Results
                                        title={languages.strings.chicksPlaced}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.broilerChicksPlaced)}
                                        />
                                    <Results
                                        title={languages.strings.broilers}
                                        cta={languages.strings.perYear}
                                        amount={this.numberWithCommas(calculations.broilersPerYear)}
                                        />
                                </View>
                        }

                        <View style={styles.resultsLine}>
                            <View>
                                <Text style={styles.labeltype}>
                                    {(production.calculateFor == 'pullets')
                                        ? languages.strings.broilers
                                        : languages.strings.pullets
                                    }
                                </Text>
                                <Text style={styles.labelsub}>
                                    {(production.calculateFor == 'pullets')
                                        ? languages.strings.perWeek
                                        : languages.strings.perYear
                                    }
                                </Text>
                            </View>
                            <View>
                                <Text style={styles.labelamount}>
                                    {(production.calculateFor == 'pullets')
                                        ? this.numberWithCommas(calculations.broilers)
                                        : this.numberWithCommas(calculations.pullets)
                                    }
                                </Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        tools: state.tools,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(ToolProduction)



const styles = StyleSheet.create({
    wrapper:{
        flex:1,
        flexDirection:'column',
        justifyContent:'flex-start',
        backgroundColor:'#F6F6F6',
    },
    mainSliderValue: {
        paddingTop: 30,
        paddingBottom: 5,
        textAlign: 'center',
        fontSize: 26
    },
    mainSliderSubTitle: {
        textAlign: 'center',
        color: '#999',
        fontSize: 14,
        paddingBottom: 10
    },
    card:{
        backgroundColor:'white',
        margin: 20,
    },
    filterButton:{
        borderColor: '#E8E8E8',
        borderBottomWidth: 1,
    },
    resultsLine:{
        borderColor: '#E8E8E8',
        borderTopWidth: 1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        minHeight: 70,
        paddingLeft:20,
        paddingRight:20,
    },
    labeltype:{
        fontSize:16,
        fontWeight:'bold',
    },
    labelsub:{
        fontSize:12,
        fontStyle: 'italic'
    },
    labelamount:{
        fontSize: 18,
    },
    track: {
      height: 4,
      borderRadius: 2,
    },
    thumb: {
      width: 20,
      height: 20,
      borderRadius: 20 / 2,
      backgroundColor: 'white',
      borderColor: '#4A4A4A',
      borderWidth: 2.5,
    },
    valuelabels:{
      flexDirection:'row',
      justifyContent:'space-between',
      paddingBottom: 10
    },
});
