import Dimensions from 'Dimensions'
import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ListView,
  WebView,
  Image,
  Navigator,
  TouchableHighlight,
  TouchableOpacity,
  LayoutAnimation,
  Animated,
  StatusBar
} from 'react-native';

import {connect} from 'react-redux'
import * as actions from '../../modules'
import { Main, listview, Filter } from '../../lib/global_styles/index.js'
import {Actions} from 'react-native-router-flux';
import RowSlider from '../../components/RowSlider'
import ButtonUpdate from '../../components/ButtonUpdate'

class ToolProductionParameters extends Component {
    constructor(props) {
      super(props);
    }

    onSliderValueChange(title, val) {
        console.log(val)
        let obj = this.props.tools.production;
        obj[title] = val;
        this.props.updateProductionTool(obj)
    }

    resetParameters() {
        this.props.resetProductionTool()
    }

    render() {
        const {tools: {production}, languages} = this.props;
        return (
            <View style={[Main.wrapperwithoutpadding]}>
                <View style={Filter.navContainer}>
                    <TouchableOpacity onPress={Actions.pop} >
                        <Image style={Filter.closeButton} source={require('./../../public/Icons/closeIcon.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.resetParameters.bind(this)}>
                        <Text style={Filter.resetButton}>{languages.strings.resetAll.toUpperCase()}</Text>
                    </TouchableOpacity>
                </View>
                <StatusBar barStyle='dark-content' />

                <View style={Filter.titlecontainer}>
                    <Text style={Filter.title}>{languages.strings.parameters.toUpperCase()}</Text>
                </View>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={[listview.container, styles.test]}>
                    <View style={{paddingBottom:50}}>
                        <RowSlider
                            onSliderValueChange={this.onSliderValueChange.bind(this, 'pullet_livability')}
                            title={languages.strings.pulletLivability}
                            type="Percentage"
                            typeLabel={languages.strings.percentage}
                            amount={production.pullet_livability}
                            maximumValue={100}
                            minimumValue={85}/>
                        <RowSlider
                            onSliderValueChange={this.onSliderValueChange.bind(this, 'hatching_eggs')}
                            title={languages.strings.hatchingEggsHensHoused}
                            type="Number"
                            typeLabel={languages.strings.number}
                            step={5}
                            amount={production.hatching_eggs}
                            maximumValue={200}
                            minimumValue={100} />
                        <RowSlider
                            onSliderValueChange={this.onSliderValueChange.bind(this, 'hatch_percent')}
                            title={languages.strings.hatchPercent}
                            type="Percentage"
                            typeLabel={languages.strings.percentage}
                            amount={production.hatch_percent}
                            maximumValue={95}
                            minimumValue={75} />
                        <RowSlider
                            onSliderValueChange={this.onSliderValueChange.bind(this, 'broiler_livability')}
                            title={languages.strings.broilerLivability}
                            type="Percentage"
                            typeLabel={languages.strings.percentage}
                            amount={production.broiler_livability}
                            maximumValue={100}
                            minimumValue={80}/>
                        <RowSlider
                            onSliderValueChange={this.onSliderValueChange.bind(this, 'broiler_doa')}
                            title={languages.strings.broilerDOA}
                            type="Percentage"
                            typeLabel={languages.strings.percentage}
                            amount={production.broiler_doa}
                            step={0.01}
                            float={true}
                            maximumValue={0.5}
                            minimumValue={0}/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        tools: state.tools,
        languages: state.languages
    }
}

export default connect(mapStateToProps, actions)(ToolProductionParameters)

const styles = StyleSheet.create({
    card: {
        flex:1,
        backgroundColor:'white',
        margin: 20,
    },
    titlecontainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#E8E8E8',
        paddingLeft: 20,
        paddingRight: 20
    },
    resetButton: {
        flex: 1
    },
    resetText: {
        fontWeight: 'bold',
        textAlign: 'right'
    },
    title: {
        fontSize: 22,
        paddingBottom: 10,
    },
    test: {
        paddingTop: 30,
        paddingLeft: 40,
        paddingRight:40,

    }
});
