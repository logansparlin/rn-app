import React, {Component} from 'react'
import { StyleSheet, View } from 'react-native';
import Dimensions from 'Dimensions'




export default class Square extends Component {
  render() {
    return (
      <View style={styles.chevroncontainer}>
        <View style={styles.square} />
      </View>

    )
  }
}

export const styles = StyleSheet.create({
  square: {
      width: 10,
      height: 30,
      backgroundColor: 'red'
  },
  chevroncontainer:{
    width: 50,
    height: 50,
    backgroundColor:'pink',
  }
});
