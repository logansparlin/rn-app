import FS from 'react-native-fs'
import store from 'react-native-simple-store'
import settings from '../settings.json'
import axios from 'axios'
import {Platform} from 'react-native'

const mainBundlePath = FS.MainBundlePath
const documentDirectory = FS.DocumentDirectoryPath;

export function saveGuideFileToCache() {
    let pathToGuidesFile = (Platform.OS == 'ios') ? `${mainBundlePath}/guides.json` : `${documentDirectory}/guides.json`;
    FS.readFile(pathToGuidesFile, 'utf8')
        .then(guides => {
            store.delete('guides')
                .then(() => {
                    store.save('guides', JSON.parse(guides))
                        .then(guides => {
                            console.log('successfully saved guides from guides.json to cache')
                        })
                        .catch(err => {
                            console.log(err)
                        })
                })
                .catch(err => {
                    console.log(err)
                })
        })
        .catch(err => {
            console.log(err)
        })
}

function addDataToLocalDB(data) {
    console.log('adding to db')
    console.log(data)

    DB.guides.erase_db(results => {
        console.log('cleared cache, adding new files')
        DB.guides.add_all(data, (results) => {
            DB.guides.get_all(function(results) {
                console.log('got all guides')
                console.log(results)
            })
        });
    });
}

export function cacheFiles() {
    console.log('caching files')
    axios.get(`${settings.api_url}/translations`)
        .then(results => {
            console.log(results.data)
            addDataToLocalDB(results.data.items)
        })
        .catch(err => {
            console.log('error')
            console.log(err)
        })
}

export function copyLocalFilesToDevice() {
    // make guides directory if it doesn't exist
    FS.mkdir(documentDirectory + "/guides")
        .then(success => {
            // move all files in local /guides folder to device /Documents folder — this provides central location to manage all local guides
            FS.readDir(mainBundlePath + '/guides')
                .then(result => {
                    if(result.length < 1) {
                        console.log('local /guides folder is empty')
                    }

                    result.map(file => {
                        FS.stat(`${documentDirectory}/guides/${file.name}`)
                            .then(res => {
                                // console.log(`${file.name} exists`)
                            })
                            .catch(err => {
                                console.log("file doesn't exist!")
                                FS.copyFile(`${mainBundlePath}/guides/${file.name}`, `${documentDirectory}/guides/${file.name}`)
                                    .then(res => {
                                        console.log('successful!')
                                    })
                                    .catch(err => {
                                        console.error(err)
                                    })
                            })
                    })
                })
                .catch(err => {
                    console.error(err)
                })
        })
        .catch(err => {
            console.log(err)
        })
}
