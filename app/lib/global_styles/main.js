import React, {Component} from 'react'
import { StyleSheet } from 'react-native';
import Dimensions from 'Dimensions'


let {height, width} = Dimensions.get('window');


let globalPadding = 20;
let headerHeight = 75;
let bottomNavHeight = 100;
let buttonHeight = 30;

let rowHeight = 225;
let rowWidth = 333;
let productheight = 190;


export const Main = StyleSheet.create({
//{/* Wapper to use for Scenes with NavTop and NavBottom */}
  wrapperwithpadding:{
    flex: 1,
    backgroundColor:'#F6F6F6',
    paddingTop: 80,
    paddingBottom: 70,
  },
 //{/* Wapper to use for full height and width Scenes */}
  wrapperwithoutpadding:{
      flex:1,
      flexDirection:'column',
      justifyContent:'flex-start',
      backgroundColor:'#ffffff',
  },
  //{/* Inset container for card components */}
  containercard:{
      backgroundColor:'white',
      margin: globalPadding,
  },
  //{/* Card shadow */}
  cardshadow:{
      shadowColor: "#000000",
      shadowOpacity: 0.05,
      shadowRadius: 2,
      zIndex: 999,
      shadowOffset: {
        height: 1,
        width: 0
      },
  },
  //{/* Inset container */}
  insetContent:{
      marginTop:globalPadding,
  },
  navContainer:{
      justifyContent:'space-between',
      alignItems:'flex-end',
      height: 90,
      flexDirection:'row',
      paddingLeft:20,
      paddingRight:20,
      paddingBottom:20,

  },
  closeButton:{
      height: 20,
      width: 20,
  },
  restButton:{
      flex:1,
  },
  checkBoxContainer:{
      flexDirection:'column',
      flexWrap:'nowrap',
      alignItems:'flex-start',
      paddingLeft:20,
      paddingRight:20,
      marginBottom:40,
  },
  titlecontainer:{
      borderBottomWidth: 1,
      borderColor: '#E8E8E8',
      paddingBottom:10,
      paddingLeft: 15,
      paddingRight:20,
  },
  content:{
      paddingTop: 20,
      paddingLeft: 20,
      paddingRight:20,
      paddingBottom: 0,
      flex:1,
  },
  title:{
      fontSize: 22,
      paddingLeft: 5,
  },
});

export const Filter = StyleSheet.create({
    navContainer:{
        justifyContent:'space-between',
        alignItems:'flex-end',
        height: 90,
        flexDirection:'row',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:20,

    },
    closeButton:{
        height: 20,
        width: 20,
    },
    restButton:{
        flex:1,
    },
    checkBoxContainer:{
        flexDirection:'column',
        flexWrap:'nowrap',
        alignItems:'flex-start',
        paddingLeft:20,
        paddingRight:20,
        marginBottom:40,
    },
    titlecontainer:{
        borderBottomWidth: 1,
        borderColor: '#E8E8E8',
        paddingBottom:10,
        paddingLeft: 15,
        paddingRight:20,
    },
    content:{
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight:20,
        paddingBottom: 0,
        flex:1,
    },
    title:{
        fontSize: 22,
        paddingLeft: 5,
    },
});


export const listview = StyleSheet.create({
  container: {
    paddingLeft: globalPadding,
    paddingRight: globalPadding,
  }
});

export const rowHero = StyleSheet.create({
    rowWrapper:{
        backgroundColor:'white',
        marginBottom: 15,
        flexDirection: 'column',
        height: rowHeight,
        borderWidth: 1,
        borderColor: '#E8E8E8',
        justifyContent:'space-between',
    },
    heroContainer:{
        flex:.66,
        overflow:'hidden',
        justifyContent:'center',
    },
    heroBackgroundImage:{
        flex:1,
        justifyContent:'center',
        width: null,
        height: null,
        resizeMode: 'cover',
        transform:[{
            scale:(.75,1.05),
        }]
    },
    heroLabel:{
        height: 70,
        backgroundColor:'rgba(209,31,62,.85)',
        justifyContent:'center',
    },
    productimage:{
        height: productheight,
        width:(productheight / 1.2),
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:25,
        right: 25,
    },

    labelLeft:{
        fontSize:22,
        color:'white',
        fontWeight: 'bold',
        right: 27,
    },
    labelRight:{
        fontSize:22,
        color:'white',
        fontWeight: 'bold',
        left: 27,
    },
    copycontainer:{
        flex: .33,
        paddingLeft: 20,
        paddingRight: 20,
        zIndex: 999,
        flexDirection: 'row',
        alignItems:'center',
        backgroundColor:'white',
        justifyContent:'space-between',
    },
    copylabel: {
        fontSize: 14,
        color:'#4A4A4A',
    },
    cobb500:{
        height: productheight,
        width:(productheight / 1.2),
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:25,
        right: 25,
    },
    cobb700:{
        height: productheight,
        width:(productheight / 1.25),
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:15,
        left: 15,
        transform:[{
            scaleX: (-1),
        }]
    },
    cobbSasso:{
        height: productheight,
        width:(productheight / .9),
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:20,
        right: 20,
    },
    toolLabel:{
        fontSize: 16,
        color:'white',
        fontWeight: 'bold',
        left:25,
        marginRight: 100,
    },
    toolIcon:{
        height: 50,
        width: 50,
        position:'absolute',
        overflow:'hidden',
        zIndex: 999,
        top:49,
        right: 20,
    },
    flexStart:{
        alignItems:'flex-start',
    },
    flexEnd:{
        alignItems:'flex-end',
    }
});
