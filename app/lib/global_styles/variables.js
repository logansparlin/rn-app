import React, {Component} from 'react'
import { StyleSheet, View } from 'react-native';
import Dimensions from 'Dimensions'


export default class Square extends Component {
  render() {
    return (
      <View style={square.square} />

    )
  }
}

const square = StyleSheet.create({
  square: {
      width: 10,
      height: 10,
      backgroundColor: 'red'
  },
});
