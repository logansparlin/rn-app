# -*- coding: utf-8 -*-

import urllib, json, requests, os

# print 'starting download'
# testfile = urllib.URLopener()
# testfile.retrieve("http://logansparlin.com/img/bg.jpg", "italy.jpg")

# print 'download complete'

print 'making API call'

if not os.path.exists('guides'):
    os.makedirs('guides')

r = requests.get('http://159.203.128.7/api/v1/translations')

guides = r.json()['items']

def convert_to_string( string ):
	return str(string)

# print convert_to_string( 'Procedimiento de vacunación' )

# urllib.URLopener().retrieve( convert_to_string("http://cobb-vantress.com/languages/guidefiles/b066e4b8-0bc7-4905-b3a1-52a904024e11_es.pdf"), 'guides/' + convert_to_string('Procedimiento de vacunación').replace(' ', '-').lower() + '.pdf' )

for guide in guides:
	# make request to pdf or html url
    if 'content' in guide.keys():
    	guiderequest = requests.get(guide['content']['url'])
    	# if status is 200 continue
    	if guiderequest.status_code == 200:
    		url = guide['content']['url'];
    		title =  guide['content']['name'];
    		#download pdf or html file
    		urllib.FancyURLopener().retrieve( convert_to_string(url), 'guides/' + title)
    		print 'saved ' + title
    	else:
    		print 'guide could not be found'
    else:
        print 'guide has no url to download'

print 'API call successful'
