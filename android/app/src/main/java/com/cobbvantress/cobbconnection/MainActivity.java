package com.cobbvantress.cobbconnection;

import com.facebook.react.ReactActivity;
import com.idehub.GoogleAnalyticsBridge.GoogleAnalyticsBridgePackage;
import com.keyee.pdfview.PDFView;
import com.calendarevents.CalendarEventsPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.rnfs.RNFSPackage;

import android.os.Bundle;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import android.content.res.AssetManager;
import android.util.Log;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "CobbConnection";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        CalendarEventsPackage.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      AssetManager assetManager = getAssets();
      String[] files = null;

      // Create /guides directory in /files directory
      File guidesDir = new File(getFilesDir() + File.separator + "guides");
      if (!guidesDir.exists()) {
          guidesDir.mkdirs();
      }

      // Move pdfs from assets/pdf folder to /files/guides folder for access within app
      try {
          files = assetManager.list("pdf");
      } catch (IOException e) {
          Log.e("tag", "Failed to get asset file list.", e);
      }

      if (files != null) for (String filename : files) {
          InputStream in = null;
          OutputStream out = null;

          try {
            in = assetManager.open("pdf/" + filename);

            File outFile = new File(getFilesDir() + File.separator + "guides", filename);

            out = new FileOutputStream(outFile);
            copyFile(in, out);
            Log.e("tag", "Copy was a success: " + outFile.getPath());
          } catch(IOException e) {
            Log.e("tag", "Failed to copy asset file: " + "pdf/" + filename, e);
          }
          finally {
              if (in != null) {
                  try {
                      in.close();
                  } catch (IOException e) {
                      // NOOP
                  }
              }
              if (out != null) {
                  try {
                      out.close();
                  } catch (IOException e) {
                      // NOOP
                  }
              }
          }
      }

      // Move guides.json file to /files folder for access within app


      Boolean guidesJsonExists = fileExists(assetManager, "guides.json");

      if(guidesJsonExists) {
          InputStream guidesIn = null;
          OutputStream guidesOut = null;
          try {
              guidesIn = assetManager.open("guides.json");

              File guidesFileOut = new File(getFilesDir(), "guides.json");
              guidesOut = new FileOutputStream(guidesFileOut);
              copyFile(guidesIn, guidesOut);
          } catch (IOException e) {
              Log.e("tag", "Failed to get asset file list.", e);
          }
          finally {
              if (guidesIn != null) {
                  try {
                      guidesIn.close();
                  } catch (IOException e) {
                      // NOOP
                  }
              }
              if (guidesOut != null) {
                 try {
                     guidesOut.close();
                 } catch (IOException e) {
                     // NOOP
                 }
              }
          }
      }
    }

    public static boolean fileExists(AssetManager assets, String path) {
        boolean assetExists = false;
        try {
            InputStream stream = assets.open(path);
            stream.close();
            assetExists = true;
        } catch (FileNotFoundException e) {
            Log.e("IOUtilities", "assetExists failed: "+e.toString());
        } catch (IOException e) {
            Log.e("IOUtilities", "assetExists failed: "+e.toString());
        }
        return assetExists;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
          out.write(buffer, 0, read);
        }
    }

    private void printFileName(String file) throws IOException {
        System.out.println("File is: " + file);
    }
}
