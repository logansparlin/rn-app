/**
* @Author: logansparlin
* @Company: Marlin
* @Date:   2016-08-01T18:09:13-05:00
* @Last modified by:   logansparlin
* @Last modified time: 2016-10-27T09:34:46-05:00
* @Flow
*/

import React, { Component } from 'react';
import {AppRegistry} from 'react-native';

import CobbConnection from './app'

AppRegistry.registerComponent('CobbConnection', () => CobbConnection);
