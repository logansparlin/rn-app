/**
* @Author: logansparlin
* @Company: Marlin
* @Date:   2016-08-01T18:09:13-05:00
* @Last modified by:   logansparlin
* @Last modified time: 2016-10-05T13:20:48-05:00
* @Flow
*/

import React, { Component } from 'react';
import {AppRegistry} from 'react-native';

import App from './app'

AppRegistry.registerComponent('CobbConnection', () => App);
